/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/ArchetypeLibrary.h>

#include <akagoria/Constants.h>
#include <akagoria/Tweaks.h>

#include <akagoria/archetypes/Ball.h>
#include <akagoria/archetypes/Bush.h>
#include <akagoria/archetypes/Hero.h>
#include <akagoria/archetypes/Tree.h>
#include <akagoria/archetypes/Well.h>

namespace akgr {

  es::Entity ArchetypeLibrary::createHero(float x, float y, float angle, const boost::filesystem::path& nanim) {
    es::Entity e = Hero::create(getManager());
    assert(e != INVALID_ENTITY);

    auto meta = getManager()->getComponent<Meta>(e);
    assert(meta);
    meta->entity = e;
    meta->name = "Kalista";

    auto animated = getManager()->getComponent<Animated>(e);
    assert(animated);
    animated->nanim.loadNanim(nanim, getResourceLoader());
    animated->animation = "static";

    auto pos = getManager()->getComponent<Position>(e);
    assert(pos);

    sf::IntRect rect;
    animated->nanim.getCurrentSprite(&rect);

    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position = { x * Tweaks::BOX2D_SCALE, y * Tweaks::BOX2D_SCALE };
    b2Body* body = getWorld()->CreateBody(&bodyDef);

    b2PolygonShape shape;
    shape.SetAsBox(
        rect.width * Tweaks::BOX2D_SCALE * 0.5f,
        rect.height * Tweaks::BOX2D_SCALE * 0.5f
    );

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.0f;
    fixtureDef.restitution = 0.0f;
    fixtureDef.filter.categoryBits = GROUND_FLOOR;
    fixtureDef.filter.maskBits = GROUND_FLOOR;

    body->CreateFixture(&fixtureDef);

    body->SetUserData(meta);
    pos->body = body;

    auto alt = getManager()->getComponent<Altitude>(e);
    assert(alt);
    alt->z = 0;

    getManager()->subscribeEntityToSystems(e);
    return e;
  }

  es::Entity ArchetypeLibrary::createBush(float x, float y, float angle, const boost::filesystem::path& img, sf::IntRect rect) {
    es::Entity e = Bush::create(getManager());
    assert(e != INVALID_ENTITY);

    sf::Texture *texture = getResourceLoader()->getTextureFromImage(img);
    assert(texture);

    auto pict = getManager()->getComponent<Picture>(e);
    assert(pict);
    pict->texture = texture;
    pict->rect = rect;

    auto pos = getManager()->getComponent<Position>(e);
    assert(pos);

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = { x * Tweaks::BOX2D_SCALE, y * Tweaks::BOX2D_SCALE };
    bodyDef.angle = angle;
    b2Body* body = getWorld()->CreateBody(&bodyDef);

    b2CircleShape circle;
    circle.m_radius = texture->getSize().y * Tweaks::BOX2D_SCALE * 0.5f / 4.0f * 0.9f;
    // there are 4 trees on each row

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circle;
    fixtureDef.filter.categoryBits = GROUND_FLOOR;
    fixtureDef.filter.maskBits = GROUND_FLOOR;

    body->CreateFixture(&fixtureDef);
    pos->body = body;

    auto alt = getManager()->getComponent<Altitude>(e);
    assert(alt);
    alt->z = 0;

    getManager()->subscribeEntityToSystems(e);
    return e;
  }

  es::Entity ArchetypeLibrary::createTree(float x, float y, float angle, const boost::filesystem::path& img, sf::IntRect rect) {
    es::Entity e = Tree::create(getManager());
    assert(e != INVALID_ENTITY);

    sf::Texture *texture = getResourceLoader()->getTextureFromImage(img);
    assert(texture);

    auto pict = getManager()->getComponent<Picture>(e);
    assert(pict);
    pict->texture = texture;
    pict->rect = rect;

    auto pos = getManager()->getComponent<Position>(e);
    assert(pos);

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = { x * Tweaks::BOX2D_SCALE, y * Tweaks::BOX2D_SCALE };
    bodyDef.angle = angle;
    b2Body* body = getWorld()->CreateBody(&bodyDef);

    b2CircleShape circle;
    circle.m_radius = texture->getSize().y * Tweaks::BOX2D_SCALE * 0.5f / 4.0f * 0.25f;
    // there are 4 trees on each row
    // 0.25f is the radius min of the trunk

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circle;
    fixtureDef.filter.categoryBits = GROUND_FLOOR;
    fixtureDef.filter.maskBits = GROUND_FLOOR;

    body->CreateFixture(&fixtureDef);
    pos->body = body;

    auto alt = getManager()->getComponent<Altitude>(e);
    assert(alt);
    alt->z = 0;

    getManager()->subscribeEntityToSystems(e);
    return e;
  }


  es::Entity ArchetypeLibrary::createWell(float x, float y, float angle, const boost::filesystem::path& img) {
    es::Entity e = Well::create(getManager());
    assert(e != INVALID_ENTITY);

    sf::Texture *texture = getResourceLoader()->getTextureFromImage(img);
    assert(texture);

    auto pict = getManager()->getComponent<Picture>(e);
    assert(pict);
    pict->texture = texture;

    auto pos = getManager()->getComponent<Position>(e);
    assert(pos);

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = { x * Tweaks::BOX2D_SCALE, y * Tweaks::BOX2D_SCALE };
    bodyDef.angle = angle;
    b2Body* body = getWorld()->CreateBody(&bodyDef);

    b2CircleShape circle;
    circle.m_radius = texture->getSize().y * Tweaks::BOX2D_SCALE * 0.5f;

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circle;
    fixtureDef.filter.categoryBits = GROUND_FLOOR;
    fixtureDef.filter.maskBits = GROUND_FLOOR;

    body->CreateFixture(&fixtureDef);
    pos->body = body;

    auto alt = getManager()->getComponent<Altitude>(e);
    assert(alt);
    alt->z = 0;

    getManager()->subscribeEntityToSystems(e);
    return e;
  }

  es::Entity ArchetypeLibrary::createBall(float x, float y, const boost::filesystem::path& img) {
    es::Entity e = Ball::create(getManager());
    assert(e != INVALID_ENTITY);

    sf::Texture *texture = getResourceLoader()->getTextureFromImage(img);
    assert(texture);

    auto pict = getManager()->getComponent<Picture>(e);
    assert(pict);
    pict->texture = texture;

    auto pos = getManager()->getComponent<Position>(e);
    assert(pos);

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = { x * Tweaks::BOX2D_SCALE, y * Tweaks::BOX2D_SCALE };
    bodyDef.angle = 0;
    b2Body* body = getWorld()->CreateBody(&bodyDef);

    b2CircleShape circle;
    circle.m_radius = texture->getSize().y * Tweaks::BOX2D_SCALE * 0.5f;

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circle;
    fixtureDef.filter.categoryBits = GROUND_FLOOR;
    fixtureDef.filter.maskBits = GROUND_FLOOR;

    body->CreateFixture(&fixtureDef);
    pos->body = body;

    auto alt = getManager()->getComponent<Altitude>(e);
    assert(alt);
    alt->z = 0;

    auto embeddable = getManager()->getComponent<Embeddable>(e);
    assert(embeddable);

    getManager()->subscribeEntityToSystems(e);
    return e;
  }

}
