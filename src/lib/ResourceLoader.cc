/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/ResourceLoader.h>

#include <cassert>

#include <boost/filesystem/fstream.hpp>
#include <tmx/TMX.h>

#include "nanim/nanim.pb.h"

namespace akgr {

  ResourceLoader::ResourceLoader(const boost::filesystem::path& basedir)
    : m_basedir(basedir)
  {
    GOOGLE_PROTOBUF_VERIFY_VERSION;
  }

  sf::Texture *ResourceLoader::getTextureFromImageAbsolute(const boost::filesystem::path& image, bool smooth) {
    sf::Texture *texture = m_textures.get(image);

    if (texture) {
      return texture;
    }

    texture = m_textures.load(image);
    texture->setSmooth(smooth);
    return texture;
  }

  sf::Font *ResourceLoader::getFontAbsolute(const boost::filesystem::path& path) {
    return m_fonts.getOrLoad(path);
  }

  tmx::Map *ResourceLoader::getMapAbsolute(const boost::filesystem::path& path) {
    return tmx::parseMapFile(path);
  }

  im::bci::nanim::Nanim *ResourceLoader::getNanimAbsolute(const boost::filesystem::path& path) {
    boost::filesystem::ifstream stream(path);

    auto nanim = new im::bci::nanim::Nanim;
    nanim->ParseFromIstream(&stream);

    return nanim;
  }

}

