/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/systems/Stuff.h>

#include <iostream>

#include <akagoria/events/Take.h>

namespace akgr {

  Stuff::Stuff(es::Manager *manager)
  : StuffBase(manager)
  {
    getManager()->registerHandler<TakeEvent>(&Stuff::onTake, this);
  }

  es::EventStatus Stuff::onTake(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == TakeEvent::type);

    TakeEvent *take = static_cast<TakeEvent*>(event);

    for (auto e : getEntities()) {
      auto embedabble = getEmbeddable(e);
      assert(embedabble);

      if (embedabble->embedded) {
        continue;
      }

      auto pos = getPosition(e);
      assert(pos);

      auto distance = b2Distance(take->pos, pos->body->GetPosition());

      std::cout << "distance: " << distance << '\n';

      if (distance < 1.0) {
        take->entity = e;

        embedabble->embedded = true;
        break;
      }
    }

    return es::EventStatus::KEEP;
  }

}
