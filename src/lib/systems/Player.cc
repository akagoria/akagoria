/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/systems/Player.h>

#include <iostream>

#include <SFML/Window/Event.hpp>

#include <ui/Action.h>

#include <akagoria/events/FloorDown.h>
#include <akagoria/events/FloorUp.h>
#include <akagoria/events/HeroPosition.h>
#include <akagoria/events/Take.h>
#include <akagoria/events/ViewDown.h>
#include <akagoria/events/ViewUp.h>

namespace akgr {

  class InputState {
  public:
    InputState(InputContext *ctx, sf::Window *window)
      : m_ctx(ctx), m_window(window)
    {
    }

    virtual ~InputState() {
    }

    virtual std::set<Player::Action> updateActions(std::set<Player::Action> actions) = 0;

    InputContext *getContext() {
      return m_ctx;
    }

    sf::Window *getWindow() {
      return m_window;
    }

  private:
    InputContext * const m_ctx;
    sf::Window * const m_window;
  };

  class InputContext {
  public:
    std::set<Player::Action> updateActions(std::set<Player::Action> actions) {
      return m_state->updateActions(actions);
    }

    void changeState(std::unique_ptr<InputState> state) {
      assert(state->getContext() == this);
      m_state = std::move(state);
    }

  private:
    std::unique_ptr<InputState> m_state;
  };

  class InputGameState : public InputState {
  public:
    InputGameState(InputContext *ctx, sf::Window *window)
      : InputState(ctx, window)
    {
      m_turn_left = std::make_shared<ui::Action>("Turn left");
      m_turn_left->addKeyControl(sf::Keyboard::Left);
      m_turn_left->setContinuous();
      m_actions.addAction(m_turn_left);

      m_turn_right = std::make_shared<ui::Action>("Turn right");
      m_turn_right->addKeyControl(sf::Keyboard::Right);
      m_turn_right->setContinuous();
      m_actions.addAction(m_turn_right);

      m_walk_forward = std::make_shared<ui::Action>("Walk forward");
      m_walk_forward->addKeyControl(sf::Keyboard::Up);
      m_walk_forward->setContinuous();
      m_actions.addAction(m_walk_forward);

      m_walk_backward = std::make_shared<ui::Action>("Walk backward");
      m_walk_backward->addKeyControl(sf::Keyboard::Down);
      m_walk_backward->setContinuous();
      m_actions.addAction(m_walk_backward);

      m_primary = std::make_shared<ui::Action>("Primary");
      m_primary->addKeyControl(sf::Keyboard::X);
      m_actions.addAction(m_primary);

      m_close = std::make_shared<ui::Action>("Close");
      m_close->addKeyControl(sf::Keyboard::Escape);
      m_close->addCloseControl();
      m_actions.addAction(m_close);
    }

    virtual std::set<Player::Action> updateActions(std::set<Player::Action> actions) override {
      sf::Event event;

      while (getWindow()->pollEvent(event)) {
        m_actions.update(event);
      }

      if (m_turn_left->isActive()) {
        actions.insert(Player::Action::TURN_LEFT);
      } else {
        actions.erase(Player::Action::TURN_LEFT);
      }

      if (m_turn_right->isActive()) {
        actions.insert(Player::Action::TURN_RIGHT);
      } else {
        actions.erase(Player::Action::TURN_RIGHT);
      }

      if (m_walk_forward->isActive()) {
        actions.insert(Player::Action::WALK_FORWARD);
      } else {
        actions.erase(Player::Action::WALK_FORWARD);
      }

      if (m_walk_backward->isActive()) {
        actions.insert(Player::Action::WALK_BACKWARD);
      } else {
        actions.erase(Player::Action::WALK_BACKWARD);
      }

      if (m_primary->isActive()) {
        actions.insert(Player::Action::TAKE_OR_TALK);
      } else {
        actions.erase(Player::Action::TAKE_OR_TALK);
      }

      if (m_close->isActive()) {
        getWindow()->close();
      }

      m_actions.reset();

      return std::move(actions);
    }

  private:
    ui::ActionSet m_actions;
    std::shared_ptr<ui::Action> m_turn_left;
    std::shared_ptr<ui::Action> m_turn_right;
    std::shared_ptr<ui::Action> m_walk_forward;
    std::shared_ptr<ui::Action> m_walk_backward;

    std::shared_ptr<ui::Action> m_primary;

    std::shared_ptr<ui::Action> m_close;
  };

  // Player implementation

  Player::Player(es::Manager *manager, sf::RenderWindow *window)
    : PlayerBase(manager), m_ctx(new InputContext), m_window(window)
  {
    std::unique_ptr<InputState> state(new InputGameState(m_ctx, m_window));
    m_ctx->changeState(std::move(state));

    manager->registerHandler<FloorDownEvent>(&Player::onFloorDown, this);
    manager->registerHandler<FloorUpEvent>(&Player::onFloorUp, this);
  }

  void Player::update(float delta) {
    auto e = getEntity();
    assert(e != INVALID_ENTITY);
    auto motion = getMotion(e);
    assert(motion);

    m_actions = m_ctx->updateActions(std::move(m_actions));

    motion->linear = LinearCommand::STOP;
    motion->angular = AngularCommand::STOP;

    for (auto action : m_actions) {
      switch (action) {
        case Action::WALK_FORWARD:
          motion->linear = LinearCommand::FORWARD;
          break;

        case Action::WALK_BACKWARD:
          motion->linear = LinearCommand::BACKWARD;
          break;

        case Action::TURN_LEFT:
          motion->angular = AngularCommand::LEFT;
          break;

        case Action::TURN_RIGHT:
          motion->angular = AngularCommand::RIGHT;
          break;

        case Action::TAKE_OR_TALK:
        {
          std::cout << "Try to take\n";

          auto pos = getPosition(e);

          TakeEvent take;
          take.pos = pos->body->GetPosition();
          take.entity = INVALID_ENTITY;

          getManager()->triggerEvent(e, &take);

          if (take.entity != INVALID_ENTITY) {
            std::cout << "Taken: " << take.entity << '\n';
          }
        }
        default:
          break;
      }
    }

  }

  void Player::postUpdate(float delta) {
    auto e = getEntity();
    assert(e != INVALID_ENTITY);

    auto pos = getPosition(e);

    HeroPositionEvent event;
    event.pos = pos->body->GetPosition();
    getManager()->triggerEvent(e, &event);
  }

  es::EventStatus Player::onFloorDown(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == FloorDownEvent::type);

    if (origin == getEntity()) {
      ViewDownEvent event;
      getManager()->triggerEvent(origin, &event);
    }

    return es::EventStatus::KEEP;
  }

  es::EventStatus Player::onFloorUp(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == FloorUpEvent::type);

    if (origin == getEntity()) {
      ViewUpEvent event;
      getManager()->triggerEvent(origin, &event);
    }

    return es::EventStatus::KEEP;
  }

}
