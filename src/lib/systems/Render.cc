/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/systems/Render.h>

#include <cassert>
#include <cmath>

#include <SFML/Graphics.hpp>

#include <akagoria/Conversion.h>
#include <akagoria/Log.h>
#include <akagoria/Tweaks.h>
#include <akagoria/events/HeroPosition.h>
#include <akagoria/events/ViewDown.h>
#include <akagoria/events/ViewUp.h>

namespace ph = std::placeholders;

namespace akgr {
  Render::Render(es::Manager *manager, sf::RenderWindow *window, b2World *world, ResourceLoader *loader, Database *database, tmx::Map *map)
    : RenderBase(manager, 1, 1)
    , m_window(window)
    , m_floor(0)
    , m_ground_map(loader, manager, map, "ground")
    , m_lo_tile_map(loader, manager, map, "low_tile")
    , m_hi_tile_map(loader, manager, map, "high_tile")
    , m_lo_sprite_map(loader, manager, map, world, database, "low_sprite")
    , m_hi_sprite_map(loader, manager, map, world, database, "high_sprite")
    , m_zone_map(world, manager, map)
  {
    manager->registerHandler<HeroPositionEvent>(&Render::onHeroPosition, this);
    manager->registerHandler<ViewDownEvent>(&Render::onViewDown, this);
    manager->registerHandler<ViewUpEvent>(&Render::onViewUp, this);
  }

  bool Render::addEntity(es::Entity e) {
    return addLocalEntity(e, 0, 0);
  }

  bool Render::removeEntity(es::Entity e) {
    return removeLocalEntity(e, 0, 0);
  }

  void Render::preUpdate(float delta) {
    getWindow()->setView(m_view);
    getWindow()->clear(sf::Color::Black);

    m_ground_map.update();
    m_lo_tile_map.update();
    m_hi_tile_map.update();
    m_lo_sprite_map.update();
    m_hi_sprite_map.update();

    getWindow()->draw(m_ground_map);
    getWindow()->draw(m_lo_tile_map);
    getWindow()->draw(m_lo_sprite_map);
  }

  void Render::updateEntity(float delta, es::Entity e) {
    auto alt = getAltitude(e);
    assert(alt);

    if (alt->z != m_floor) {
      return;
    }

    auto pos = getPosition(e);
    assert(pos);

    auto pict = getPicture(e);
    assert(pict);

    sf::Sprite sprite;

    sprite.setTexture(*pict->texture);

    if (pict->rect.width != 0 && pict->rect.height != 0) {
      sprite.setTextureRect(pict->rect);
    }

    sf::FloatRect bounds = sprite.getLocalBounds();
    sprite.setOrigin(bounds.width * 0.5, bounds.height * 0.5);

    sprite.setPosition(realCoordsFromBoxCoords(pos->body->GetPosition()));
    sprite.setRotation(pos->body->GetAngle() / M_PI_2 * 90);

    getWindow()->draw(sprite);
  }

  void Render::postUpdate(float delta) {
    getWindow()->draw(m_hi_tile_map);
    getWindow()->draw(m_hi_sprite_map);
    getWindow()->display();
  }

  es::EventStatus Render::onHeroPosition(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == HeroPositionEvent::type);

    m_view.setSize(sf::Vector2f(getWindow()->getSize()));

    sf::Vector2f pos = realCoordsFromBoxCoords(static_cast<HeroPositionEvent*>(event)->pos);
//     float angle = static_cast<HeroPositionEvent*>(event)->angle;

//     if (getState()->getPointOfView() == PointOfView::FIRST_PERSON) {
//       m_view.setCenter(pos);
//       m_view.setRotation(angle / M_PI_2 * 90.0f);
//     } else {

#if 0
      sf::Vector2f diff = pos - m_view.getCenter();
      sf::Vector2f half = m_view.getSize() / 2.0f - sf::Vector2f(Tweaks::BORDER, Tweaks::BORDER);

      if (diff.x < - half.x) {
        m_view.move(diff.x + half.x, 0.);
      }

      if (diff.x > half.x) {
        m_view.move(diff.x - half.x, 0.);
      }

      if (diff.y < - half.y) {
        m_view.move(0., diff.y + half.y);
      }

      if (diff.y > half.y) {
        m_view.move(0., diff.y - half.y);
      }
#endif

      m_view.setCenter(pos);
      m_view.setRotation(0.);
//     }

//     m_view.zoom(getState()->getZoom());

    m_view.setCenter(pos);
    return es::EventStatus::KEEP;
  }

  es::EventStatus Render::onViewDown(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == ViewDownEvent::type);
    m_floor--;
    return es::EventStatus::KEEP;
  }

  es::EventStatus Render::onViewUp(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == ViewUpEvent::type);
    m_floor++;
    return es::EventStatus::KEEP;
  }

}
