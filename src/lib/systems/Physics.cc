/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/systems/Physics.h>

#include <algorithm>
#include <cassert>
#include <cmath>

#include <akagoria/Constants.h>
#include <akagoria/Log.h>
#include <akagoria/Tweaks.h>
#include <akagoria/events/FloorDown.h>
#include <akagoria/events/FloorUp.h>

namespace ph = std::placeholders;

namespace akgr {

    Physics::Physics(es::Manager *manager, b2World *world)
      : PhysicsBase(manager)
      , m_world(world)
    {
      manager->registerHandler<FloorDownEvent>(&Physics::onFloorDown, this);
      manager->registerHandler<FloorUpEvent>(&Physics::onFloorUp, this);
    }

  void Physics::updateEntity(float delta, es::Entity e) {
    auto pos = getPosition(e);
    assert(pos);

    auto motion = getMotion(e);
    assert(motion);


    float angle = pos->body->GetAngle();

    switch (motion->angular) {
      case AngularCommand::LEFT:
        angle -= delta * Tweaks::TURN;
        break;

      case AngularCommand::RIGHT:
        angle += delta * Tweaks::TURN;

      default:
        break;
    }

    pos->body->SetTransform(pos->body->GetPosition(), angle);
    pos->body->SetAngularVelocity(0.0f);

    b2Rot rot(angle);
    b2Vec2 velocity(0.0f, 0.0f);

    switch (motion->linear) {
      case LinearCommand::FORWARD:
        velocity.y = - Tweaks::HOP * Tweaks::BOX2D_SCALE;
        break;

      case LinearCommand::BACKWARD:
        velocity.y = Tweaks::HOP * Tweaks::BOX2D_SCALE * 0.5;
        break;

      default:
        break;
    }

    velocity = b2Mul(rot, velocity);
    pos->body->SetLinearVelocity(velocity);
  }

  void Physics::postUpdate(float delta) {
    int32 velocityIterations = 10; // 6;
    int32 positionIterations = 8; // 2;
    getWorld()->Step(delta, velocityIterations, positionIterations);
  }

  es::EventStatus Physics::onFloorDown(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == FloorDownEvent::type);

    auto pos = getPosition(origin);
    assert(pos);

    b2Body *body = pos->body;

    for (b2Fixture *fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext()) {
      b2Filter filter = fixture->GetFilterData();
      assert(filter.categoryBits == filter.maskBits);

      if (filter.categoryBits == MIN_FLOOR) {
        Log::warning("Trying to go under the minimum floor\n");
        break;
      }

      filter.categoryBits >>= 1;
      filter.maskBits >>= 1;
      fixture->SetFilterData(filter);
    }

    Altitude *alt = getAltitude(origin);
    alt->z--;

    return es::EventStatus::KEEP;
  }

  es::EventStatus Physics::onFloorUp(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == FloorUpEvent::type);

    auto pos = getPosition(origin);
    assert(pos);

    b2Body *body = pos->body;

    for (b2Fixture *fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext()) {
      b2Filter filter = fixture->GetFilterData();
      assert(filter.categoryBits == filter.maskBits);

      if (filter.categoryBits == MAX_FLOOR) {
        Log::warning("Trying to go above the maximum floor\n");
        break;
      }

      filter.categoryBits <<= 1;
      filter.maskBits <<= 1;
      fixture->SetFilterData(filter);
    }

    Altitude *alt = getAltitude(origin);
    alt->z++;

    return es::EventStatus::KEEP;
  }

}
