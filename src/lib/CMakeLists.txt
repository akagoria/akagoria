include_directories(${Boost_INCLUDE_DIRS})
include_directories(${LIBES0_INCLUDE_DIRS})
include_directories(${LIBSUIT0_INCLUDE_DIRS})
include_directories(${LIBTMX0_INCLUDE_DIRS})
include_directories(${SFML2_INCLUDE_DIRS})
include_directories(${BOX2D_INCLUDE_DIRS})
include_directories(${PROTOBUF_INCLUDE_DIRS})
include_directories(${YAMLCPP_INCLUDE_DIRS})

set(LIBAKAGORIA_SRC
  ArchetypeLibrary.cc
  Basic.cc
  Conversion.cc
  Database.cc
  Log.cc
  Nanim.cc
  ResourceLoader.cc
  map/GridMap.cc
  map/SpriteMap.cc
  map/TileMap.cc
  map/ZoneMap.cc
  nanim/nanim.pb.cc
  systems/Animation.cc
  systems/Movement.cc
  systems/Physics.cc
  systems/Player.cc
  systems/Render.cc
  systems/Stuff.cc
)

add_library(akagoria0 SHARED
  ${LIBAKAGORIA_SRC}
)

target_link_libraries(akagoria0
  ${Boost_LIBRARIES}
  ${LIBES0_LIBRARIES}
  ${LIBSUIT0_LIBRARIES}
  ${LIBTMX0_LIBRARIES}
  ${SFML2_LIBRARIES}
  ${BOX2D_LIBRARIES}
  ${PROTOBUF_LIBRARIES}
  ${YAMLCPP_LIBRARIES}
)

set_target_properties(akagoria0
  PROPERTIES
  VERSION ${CPACK_PACKAGE_VERSION}
  SOVERSION ${CPACK_PACKAGE_VERSION_MAJOR}
)

# message("CMAKE_INSTALL_LIBDIR: ${CMAKE_INSTALL_LIBDIR}")

install(
  TARGETS akagoria0
  LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
  ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
)
