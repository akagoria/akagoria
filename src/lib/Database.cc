/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/Database.h>

#include <yaml-cpp/yaml.h>

#include <akagoria/Log.h>

namespace akgr {

  void Database::load(const boost::filesystem::path& basedir) {
    Log::info("Loading database\n");

    try {
      boost::filesystem::path path = basedir / "data/collisions.yml";
      YAML::Node node = YAML::LoadFile(path.string());

      assert(node.IsMap());

      for (const auto& entry : node) {
        std::string name = entry.first.as<std::string>();
        auto properties = entry.second;

        assert(properties.IsMap());

        auto type_node = properties["type"];

        if (!type_node) {
          Log::warning("Missing type for entry: '%s'\n", name.c_str());
          continue;
        }

        std::string type = type_node.as<std::string>();

        if (type == "circle") {
          auto radius_node = properties["radius"];
          assert(radius_node);

          auto radius = radius_node.as<float>();

          std::unique_ptr<CollisionCircleData> data(new CollisionCircleData);
          data->shape = CollisionShape::CIRCLE;
          data->radius = radius;

          m_collisions.emplace(name, std::move(data));

        } else {
          Log::warning("Unknown type for entry: '%s'\n", name.c_str());
        }

      }


    } catch (std::exception& ex) {
      Log::error("Error when loading database: %s\n", ex.what());
      return;
    }

  }

  CollisionData *Database::getCollisionDataFor(const std::string& name) {
    auto it = m_collisions.find(name);

    if (it == m_collisions.end()) {
      return nullptr;
    }

    return it->second.get();
  }

}
