/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/Conversion.h>

#include <akagoria/Tweaks.h>
#include <akagoria/Constants.h>

namespace akgr {

  sf::Vector2f realCoordsFromBoxCoords(const b2Vec2& pos) {
    return { pos.x / Tweaks::BOX2D_SCALE, pos.y / Tweaks::BOX2D_SCALE };
  }

  uint16_t bitsFromFloor(int floor) {
    uint16_t bits;

    switch (floor) {
      case 3:
        bits = THIRD_FLOOR;
        break;
      case 2:
        bits = SECOND_FLOOR;
        break;
      case 1:
        bits = FIRST_FLOOR;
        break;
      case 0:
        bits = GROUND_FLOOR;
        break;
      case -1:
        bits = FIRST_BASEMENT;
        break;
      case -2:
        bits = SECOND_BASEMENT;
        break;
      case -3:
        bits = THIRD_BASEMENT;
        break;
      default:
        bits = GROUND_FLOOR;
        break;
    }

    return bits;
  }

  uint16_t bitsFromFloor(const std::string& key) {
    int floor = std::stoi(key);
    return bitsFromFloor(floor);
  }

}
