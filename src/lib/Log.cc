/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/Log.h>

#include <cstdio>

namespace akgr {

  static const char *stringFromLevel(Log::Level level) {
    switch (level) {
      case Log::Level::DEBUG:
        return "D";
      case Log::Level::INFO:
        return "I";
      case Log::Level::WARNING:
        return "W";
      case Log::Level::ERROR:
        return "E";
      case Log::Level::FATAL:
        return "F";
    }

    return "?";
  }

  Log::Level Log::s_current_level = Log::Level::INFO;

  void Log::setCurrentLevel(Level level) {
    s_current_level = level;
  }

  void Log::log(Level level, const char *fmt, va_list ap) {
    if (level < s_current_level) {
      return;
    }

    std::fprintf(stderr, "%s: ", stringFromLevel(level));
    std::vfprintf(stderr, fmt, ap);
  }

}
