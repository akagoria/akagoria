/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/map/SpriteMap.h>

#include <tmx/ObjectLayer.h>

#include <akagoria/Conversion.h>
#include <akagoria/Log.h>
#include <akagoria/Tweaks.h>

namespace ph = std::placeholders;

namespace akgr {

  namespace {

    /*
     * A map layer visitor
     */
    class SpriteConstructor : public tmx::LayerVisitor {
    public:
      SpriteConstructor(tmx::Map *_map, ResourceLoader *_loader, b2World *_world, Database *_database, const std::string& kind, SpriteMap *_spritemap)
        : map(_map), loader(_loader), world(_world), database(_database), ref_kind(kind), spritemap(_spritemap)
      {  }

      virtual void  visitObjectLayer(tmx::ObjectLayer &layer) override {
        if (!layer.hasProperty("kind")) {
          Log::warning("No kind for the layer: '%s'\n", layer.getName().c_str());
          return;
        }

        const std::string& kind = layer.getProperty("kind", "");

        if (kind != ref_kind) {
          return;
        }

        int floor = std::stoi(layer.getProperty("floor", "0"));

        Log::info("\tLoading sprite layer: '%s' (floor: %i)\n", layer.getName().c_str(), floor);

        unsigned count = 0;
        for (auto obj : layer) {
          const std::string& name = obj->getName();

          if (!obj->isTile()) {
            Log::warning("Object is not a tile: '%s'\n", name.c_str());
            continue;
          }

          auto tile = static_cast<const tmx::TileObject *>(obj);

          unsigned gid = tile->getGID();
          assert(gid != 0);

          auto tileset = map->getTileSetFromGID(gid);
          auto image = tileset->getImage();
          sf::Texture *texture = loader->getTextureFromImageAbsolute(image->getSource().string(), true);
          assert(texture);

          tmx::Size size;
          if (image->hasSize()) {
            size = image->getSize();
          } else {
            sf::Vector2u texture_size = texture->getSize();
            size.width = texture_size.x;
            size.height = texture_size.y;
          }

          gid = gid - tileset->getFirstGID();
          tmx::Rect rect = tileset->getCoords(gid, size);

          Sprite sprite;

          sprite.z = floor;

          sprite.pos.x = tile->getX();
          sprite.pos.y = tile->getY() - tileset->getTileHeight();

          sprite.rect.left = rect.x;
          sprite.rect.top = rect.y;
          sprite.rect.width = rect.width;
          sprite.rect.height = rect.height;

          sprite.texture = texture;

          spritemap->addSprite(sprite);
          count++;

          auto collision_data = database->getCollisionDataFor(name);

          if (!collision_data) {
            continue;
          }

          float x = sprite.pos.x + rect.width / 2;
          float y = sprite.pos.y + rect.height / 2;

          b2BodyDef bodyDef;
          bodyDef.type = b2_staticBody;
          bodyDef.position = { x * Tweaks::BOX2D_SCALE, y * Tweaks::BOX2D_SCALE };
          b2Body* body = world->CreateBody(&bodyDef);

          b2FixtureDef fixtureDef;
          fixtureDef.filter.categoryBits = bitsFromFloor(floor);
          fixtureDef.filter.maskBits = bitsFromFloor(floor);

          if (collision_data->shape == CollisionShape::CIRCLE) {
            auto circle_data = static_cast<CollisionCircleData*>(collision_data);
            float radius = circle_data->radius;

            b2CircleShape circle;
            circle.m_radius = radius * Tweaks::BOX2D_SCALE;

            fixtureDef.shape = &circle;

            body->CreateFixture(&fixtureDef);
          } else {
            Log::warning("Unknown collision data shape\n");
          }
        }

        Log::info("\t\tSprites loaded: %u\n", count);
      }

      tmx::Map *map;
      ResourceLoader *loader;
      b2World *world;
      Database *database;
      const std::string& ref_kind;
      SpriteMap *spritemap;
    };

  }

  #define UNIT 25

  SpriteMap::SpriteMap(ResourceLoader *loader, es::Manager *manager, tmx::Map *map, b2World *world, Database *database, const std::string& kind)
    : GridMap<Sprite>(manager, map, UNIT)
  {
    Log::info("Loading sprite layers (kind: %s)\n", kind.c_str());
    SpriteConstructor visitor(map, loader, world, database, kind, this);
    map->visitLayers(visitor);

    setDirty();
  }

  void SpriteMap::addSprite(const Sprite& sprite) {
    addObject(sprite, sprite.pos);
  }

  void SpriteMap::update() {
    if (!isDirty()) {
      return;
    }

//     Log::info("SpriteMap update!\n");

    m_sprites.clear();
    processObjects([this](const Sprite& sprite) {
      if (sprite.z == getFloor()) {
        m_sprites.push_back(&sprite);
      }
    });

    std::sort(m_sprites.begin(), m_sprites.end(), [](const Sprite *lhs, const Sprite *rhs) {
      return lhs->texture;
    });

//     Log::info("\tDone! (%zu)\n", m_sprites.size());

    setClean();
  }

  void SpriteMap::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    for (auto sprite : m_sprites) {
      sf::Sprite local_sprite(*sprite->texture, sprite->rect);
      local_sprite.setPosition(sprite->pos);
      target.draw(local_sprite, states);
    }
  }

}
