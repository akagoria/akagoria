/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/map/TileMap.h>

#include <tmx/TileLayer.h>
#include <akagoria/Log.h>

namespace ph = std::placeholders;

namespace akgr {

  namespace {

    /*
     * A map layer visitor
     */
    class TileConstructor : public tmx::LayerVisitor {
    public:
      TileConstructor(tmx::Map *_map, ResourceLoader *_loader, const std::string& kind, TileMap *_tilemap)
        : map(_map), loader(_loader), ref_kind(kind), tilemap(_tilemap)
      {  }

      virtual void visitTileLayer(tmx::TileLayer& layer) override {
        if (!layer.hasProperty("kind")) {
          Log::warning("No kind for the layer: '%s'\n", layer.getName().c_str());
          return;
        }

        const std::string& kind = layer.getProperty("kind", "");

        if (kind != ref_kind) {
          return;
        }

        int floor = std::stoi(layer.getProperty("floor", "0"));

        Log::info("\tLoading tile layer: '%s' (floor: %i)\n", layer.getName().c_str(), floor);

        unsigned tilewidth = map->getTileWidth();
        assert(tilewidth);

        unsigned tileheight = map->getTileHeight();
        assert(tileheight);

        unsigned width = map->getWidth();
        assert(width);

        unsigned height = map->getHeight();
        assert(height);


        sf::Texture *texture = nullptr;
        tmx::Size size;

        unsigned k = 0;
        unsigned count = 0;
        for (auto cell : layer) {
          unsigned i = k % width;
          unsigned j = k / width;
          assert(j < height);

          unsigned gid = cell.getGID();

          if (gid == 0) {
            k++;
            continue;
          }

          auto tileset = map->getTileSetFromGID(gid);

          if (texture == nullptr) {
            assert(tileset->getTileWidth() == tilewidth);
            assert(tileset->getTileHeight() == tileheight);

            auto image = tileset->getImage();
            texture = loader->getTextureFromImageAbsolute(image->getSource().string(), true);
            assert(texture);

            if (image->hasSize()) {
              size = image->getSize();
            } else {
              sf::Vector2u texture_size = texture->getSize();
              size.width = texture_size.x;
              size.height = texture_size.y;
            }

            tilemap->setTexture(texture);
          } else {
            assert(texture == loader->getTextureFromImageAbsolute(tileset->getImage()->getSource().string(), true));
          }

          gid = gid - tileset->getFirstGID();
          tmx::Rect rect = tileset->getCoords(gid, size);

          Tile tile;

          tile.z = floor;

          // define its 4 corners
          tile.position[0] = sf::Vector2f(i * tilewidth, j * tileheight);
          tile.position[1] = sf::Vector2f((i + 1) * tilewidth, j * tileheight);
          tile.position[2] = sf::Vector2f((i + 1) * tilewidth, (j + 1) * tileheight);
          tile.position[3] = sf::Vector2f(i * tilewidth, (j + 1) * tileheight);

          // define its 4 texture coordinates
          tile.texCoords[0] = sf::Vector2f(rect.x, rect.y);
          tile.texCoords[1] = sf::Vector2f(rect.x + rect.width, rect.y);
          tile.texCoords[2] = sf::Vector2f(rect.x + rect.width, rect.y + rect.height);
          tile.texCoords[3] = sf::Vector2f(rect.x, rect.y + rect.height);

          tilemap->addTile(tile);
          count++;

          k++;
        }

        Log::info("\t\tTiles loaded: %u\n", count);
      }

      tmx::Map *map;
      ResourceLoader *loader;
      const std::string& ref_kind;
      TileMap *tilemap;
    };

  }

  #define UNIT 25

  TileMap::TileMap(ResourceLoader *loader, es::Manager *manager, tmx::Map *map, const std::string& kind)
    : GridMap<Tile>(manager, map, UNIT)
    , m_texture(nullptr)
  {
    Log::info("Loading tile layers (kind: %s)\n", kind.c_str());
    TileConstructor visitor(map, loader, kind, this);
    map->visitLayers(visitor);

    setDirty();
  }

  void TileMap::addTile(const Tile& tile) {
    addObject(tile, tile.position[0]);
  }

  void TileMap::setTexture(sf::Texture *texture) {
    assert(m_texture == nullptr || texture == m_texture);
    m_texture = texture;
  }

  void TileMap::update() {
    if (!isDirty()) {
      return;
    }

//     Log::info("TileMap update!\n");

    m_vertices.clear();
    m_vertices.setPrimitiveType(sf::Quads);

    processObjects([this](const Tile& tile) {
      if (tile.z == getFloor()) {
        for (std::size_t i = 0; i < 4; ++i) {
          m_vertices.append(sf::Vertex(tile.position[i], tile.texCoords[i]));
        }
      }
    });

//     Log::info("\tDone! (%u)\n", m_vertices.getVertexCount());

    setClean();
  }

  void TileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    if (!m_texture) {
      return;
    }

    states.transform *= getTransform();
    states.texture = m_texture;
    target.draw(m_vertices, states);
  }

}
