/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <akagoria/map/ZoneMap.h>

#include <tmx/ObjectLayer.h>
#include <tmx/LayerVisitor.h>

#include <akagoria/Constants.h>
#include <akagoria/Conversion.h>
#include <akagoria/Log.h>
#include <akagoria/Tweaks.h>
#include <akagoria/components/Altitude.h>
#include <akagoria/components/Meta.h>

namespace akgr {

  namespace {

    class ZoneConstructor : public tmx::LayerVisitor {
    public:
      ZoneConstructor(ZoneMap *_zonemap)
        : zonemap(_zonemap)
      {
      }

      virtual void  visitObjectLayer(tmx::ObjectLayer &layer) override {
        if (!layer.hasProperty("kind")) {
          Log::warning("No kind for the layer: '%s'\n", layer.getName().c_str());
          return;
        }

        const std::string& kind = layer.getProperty("kind", "");

        if (kind != "zone") {
          return;
        }

        auto floor = layer.getProperty("floor", "0");
        uint16_t floor_bits = bitsFromFloor(floor);

        Log::info("\tLoading zone layer: '%s' (floor: %s)\n", layer.getName().c_str(), floor.c_str());

        unsigned event_count = 0;
        unsigned collision_count = 0;
        for (auto obj : layer) {
          const std::string& name = obj->getName();

          if (obj->getType() == "event") {
            if (!obj->hasProperty("type")) {
              Log::warning("No event type for the object: '%s'\n", name.c_str());
              continue;
            }

            const std::string& type = obj->getProperty("type", "");
            zonemap->addEventZone(obj, floor_bits, name, es::Hash(type));
            event_count++;
          } else if (obj->getType() == "collision") {
            zonemap->addCollisionZone(obj, floor_bits);
            collision_count++;
          } else {
            Log::warning("No type for the object: '%s'\n", name.c_str());
          }

        }

        Log::info("\t\tObjects loaded: %u event zones and %u collision zone\n", event_count, collision_count);
      }

      ZoneMap * const zonemap;
    };


  }

  static bool operator<(const ZoneMap::EventZone& lhs, const ZoneMap::EventZone& rhs) {
    return lhs.fixture < rhs.fixture;
  }

  ZoneMap::ZoneMap(b2World *world, es::Manager *manager, tmx::Map *map)
    : m_world(world)
    , m_manager(manager)
  {
    // load the map
    Log::info("Loading zone layers\n");
    ZoneConstructor visitor(this);
    map->visitLayers(visitor);
    m_world->SetContactListener(this);
  }

  void ZoneMap::addEventZone(const tmx::Object *object, uint16_t floor_bits, const std::string& name, es::EventType event) {
    b2Fixture *fixture = createFixtureFromObject(object, floor_bits, FixtureType::SENSOR);

    if (fixture == nullptr) {
      Log::warning("A zone could not be transformed into a fixture: '%s'\n", name.c_str());
      return;
    }

    m_zones.insert({ event, true, fixture });

    if (!name.empty()) {
      m_named_zones.emplace(name, fixture);
    }
  }

  void ZoneMap::addCollisionZone(const tmx::Object *object, uint16_t floor_bits) {
    b2Fixture *fixture = createFixtureFromObject(object, floor_bits, FixtureType::SOLID);

    if (fixture == nullptr) {
      Log::warning("A zone could not be transformed into a fixture\n");
      return;
    }
  }

  void ZoneMap::BeginContact(b2Contact *contact) {
    b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();

    triggerZoneEvent(fixtureB, fixtureA) || triggerZoneEvent(fixtureA, fixtureB);
  }

  bool ZoneMap::triggerZoneEvent(b2Fixture *fixtureZone, b2Fixture *fixtureOrigin) {
    auto zone = findZone(fixtureZone);

    if (zone == nullptr) {
      return false;
    }

    Meta *meta = static_cast<Meta*>(fixtureOrigin->GetBody()->GetUserData());
    assert(meta);
    Log::info("Event triggered: %lu\n", zone->event);
    m_manager->triggerEvent(meta->entity, zone->event, nullptr);
    return true;
  }

  const ZoneMap::EventZone *ZoneMap::findZone(b2Fixture *fixture) const {
    EventZone model{INVALID_EVENT, false, fixture};
    auto it = m_zones.find(model);

    if (it == m_zones.end()) {
      return nullptr;
    }

    return it->active ? &(*it) : nullptr;
  }

  b2Fixture *ZoneMap::createFixtureFromObject(const tmx::Object *object, uint16_t floor_bits, FixtureType type) {
    switch (object->getKind()) {
      case tmx::Object::Kind::RECTANGLE:
        return createFixtureFromRectangle(static_cast<const tmx::Rectangle*>(object), floor_bits, type);
      case tmx::Object::Kind::POLYLINE:
      case tmx::Object::Kind::POLYGON:
        return createFixtureFromPoly(static_cast<const tmx::PolyBase*>(object), floor_bits, type);
      default:
        break;
    }

    return nullptr;
  }

  b2Fixture *ZoneMap::createFixtureFromRectangle(const tmx::Rectangle *rect, uint16_t floor_bits, FixtureType type) {
    float x = rect->getX();
    float y = rect->getY();
    float width = rect->getWidth();
    float height = rect->getHeight();

    x += width * 0.5f;
    y += height * 0.5f;

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = { x * Tweaks::BOX2D_SCALE, y * Tweaks::BOX2D_SCALE };
    b2Body* body = m_world->CreateBody(&bodyDef);

    b2PolygonShape shape;
    shape.SetAsBox(
        width * Tweaks::BOX2D_SCALE * 0.5f,
        height * Tweaks::BOX2D_SCALE * 0.5f
    );

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.isSensor = (type == FixtureType::SENSOR);
    fixtureDef.filter.categoryBits = floor_bits;
    fixtureDef.filter.maskBits = floor_bits;

    return body->CreateFixture(&fixtureDef);
  }

  b2Fixture *ZoneMap::createFixtureFromPoly(const tmx::PolyBase *poly, uint16_t floor_bits, FixtureType type) {
    float x = poly->getX();
    float y = poly->getY();

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = { x * Tweaks::BOX2D_SCALE, y * Tweaks::BOX2D_SCALE };
    b2Body *body = m_world->CreateBody(&bodyDef);

    std::vector<b2Vec2> line;
    for (auto point : *poly) {
      line.emplace_back(point.x * Tweaks::BOX2D_SCALE, point.y * Tweaks::BOX2D_SCALE);
    }

    b2ChainShape shape;

    if (poly->isPolygon()) {
      shape.CreateLoop(line.data(), line.size());
    } else {
      shape.CreateChain(line.data(), line.size());
    }

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.isSensor = (type == FixtureType::SENSOR);
    fixtureDef.filter.categoryBits = floor_bits;
    fixtureDef.filter.maskBits = floor_bits;

    return body->CreateFixture(&fixtureDef);
  }

}

