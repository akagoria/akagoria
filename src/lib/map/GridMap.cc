/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/map/GridMap.h>

#include <akagoria/Conversion.h>
#include <akagoria/events/HeroPosition.h>
#include <akagoria/events/ViewDown.h>
#include <akagoria/events/ViewUp.h>

namespace ph = std::placeholders;

namespace akgr {

  GridMapBase::GridMapBase(es::Manager *manager, tmx::Map *map, unsigned unit)
  : m_floor(0)
  , m_unitwidth(map->getTileWidth() * unit)
  , m_unitheight(map->getTileHeight() * unit)
  , m_x(0)
  , m_y(0)
  , m_dirty(true)
  {
    manager->registerHandler<HeroPositionEvent>(&GridMapBase::onHeroPosition, this);
    manager->registerHandler<ViewDownEvent>(&GridMapBase::onViewDown, this);
    manager->registerHandler<ViewUpEvent>(&GridMapBase::onViewUp, this);
  }

  void GridMapBase::updateFocus(const sf::Vector2f& pos) {
    assert(pos.x >= 0.0f);
    assert(pos.y >= 0.0f);

    unsigned x = pos.x / m_unitwidth;
    unsigned y = pos.y / m_unitheight;

    updateFocus(x, y);
  }

  void GridMapBase::updateFocus(unsigned x, unsigned y) {
    if (x != m_x || y != m_y) {
      m_x = x;
      m_y = y;
      setDirty();
    }
  }

  es::EventStatus GridMapBase::onHeroPosition(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == HeroPositionEvent::type);

    sf::Vector2f pos = realCoordsFromBoxCoords(static_cast<HeroPositionEvent *>(event)->pos);
    updateFocus(pos);

    return es::EventStatus::KEEP;
  }

  es::EventStatus GridMapBase::onViewDown(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == ViewDownEvent::type);
    m_floor--;
    setDirty();
    return es::EventStatus::KEEP;
  }

  es::EventStatus GridMapBase::onViewUp(es::Entity origin, es::EventType type, es::Event *event) {
    assert(type == ViewUpEvent::type);
    m_floor++;
    setDirty();
    return es::EventStatus::KEEP;
  }

}
