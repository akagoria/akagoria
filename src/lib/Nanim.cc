/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <akagoria/Nanim.h>

#include <cassert>
#include <akagoria/ResourceLoader.h>
#include <akagoria/Log.h>

#include "nanim/nanim.pb.h"

namespace akgr {

  Nanim::~Nanim() {
    delete m_nanim;

    for (auto item : m_textures) {
      delete item.second;
    }
  }

  void Nanim::loadNanim(const boost::filesystem::path& path, ResourceLoader *loader) {
    if (m_nanim) {
      delete m_nanim;
    }

    m_textures.clear();

    m_nanim = loader->getNanim(path);
    assert(m_nanim);

    Log::info("Loading nanim: '%s'\n", path.c_str());

    for (auto img : m_nanim->images()) {
      assert(img.has_name());
      assert(img.has_pixels());

      const sf::Uint8 *data = reinterpret_cast<const sf::Uint8 *>(img.pixels().c_str());
      assert(img.pixels().length() == static_cast<std::size_t>(4 * img.width() * img.height()));

      Log::info("\tLoading image: '%s' (%ix%i)\n", img.name().c_str(), img.width(), img.height());

      auto tex = new sf::Texture;
      tex->create(img.width(), img.height());
      tex->update(data);
      tex->setSmooth(true);

      m_textures.emplace(img.name(), tex);
    }

  }

  bool Nanim::updateAnimation(const std::string& name, float dt) {
    assert(m_nanim);
    assert(m_nanim->animations_size() > 0);

    bool ret = false;

    if (name != m_nanim->animations(m_current_animation).name()) {
      changeCurrentAnimation(name);
      ret = true;
    }

    assert(m_current_animation != -1);
    auto& animation = m_nanim->animations(m_current_animation);
    assert(animation.frames_size() > 0);

    m_time -= dt;

    while (m_time < 0) {
      m_current_frame = (m_current_frame + 1) % animation.frames_size();
      auto& frame = animation.frames(m_current_frame);
      m_time += frame.duration() / 1000.0f;
      ret = true;
    }

    return ret;
  }

  sf::Texture *Nanim::getCurrentSprite(sf::IntRect *rect) {
    assert(m_nanim);
    assert(m_nanim->animations_size() > 0);

    auto& animation = m_nanim->animations(m_current_animation);
    assert(animation.frames_size() > 0);

    assert(m_current_frame < animation.frames_size());
    assert(m_current_frame >= 0);
    auto& frame = animation.frames(m_current_frame);

    assert(frame.has_imagename());
    auto it = m_textures.find(frame.imagename());
    assert(it != m_textures.end());

    sf::Texture *tex = it->second;

    if (rect) {
      sf::Vector2u size = tex->getSize();
      float u1 = frame.u1();
      float v1 = frame.v1();
      float u2 = frame.u2();
      float v2 = frame.v2();

      rect->left = size.x * u1;
      rect->top = size.y * v1;
      rect->width = size.x * (u2 - u1);
      rect->height = size.y * (v2 - v1);
    }

    return tex;
  }

  void Nanim::changeCurrentAnimation(const std::string& name) {
    assert(m_nanim);
    assert(m_nanim->animations_size() > 0);

    m_time = 0.0f;
    m_current_frame = -1;

    for (int i = 0; i < m_nanim->animations_size(); ++i) {
      auto& animation = m_nanim->animations(i);
      if (animation.name() == name) {
        m_current_animation = i;
        return;
      }
    }

    m_current_animation = -1;
  }

}
