/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>
#include <cstdlib>
#include <cstdio>

#include <boost/filesystem.hpp>
#include <Box2D/Box2D.h>
#include <es/Manager.h>
#include <SFML/Graphics/RenderWindow.hpp>

#include <akagoria/ArchetypeLibrary.h>
#include <akagoria/Basic.h>
#include <akagoria/Constants.h>
#include <akagoria/Database.h>
#include <akagoria/ResourceLoader.h>
#include <akagoria/Tweaks.h>
#include <akagoria/components/Position.h>
#include <akagoria/events/HeroPosition.h>
#include <akagoria/systems/Animation.h>
#include <akagoria/systems/Movement.h>
#include <akagoria/systems/Physics.h>
#include <akagoria/systems/Player.h>
#include <akagoria/systems/Render.h>
#include <akagoria/systems/Stuff.h>

#include "config.h"

#define WIDTH 1024
#define HEIGHT 576

int main() {
  std::printf("Akagoria, the revenge of Kalista\nVersion %s\n", AKAGORIA_VERSION);

  /*
   * Splash screen
   */

  akgr::ResourceLoader loader(AKAGORIA_DATADIR);

  sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "Akagoria, the revenge of Kalista");

  window.clear(sf::Color::White);

  // define a splash message
  sf::Text text;
  text.setFont(*loader.getFont("fonts/DejaVuSans.ttf"));
  text.setCharacterSize(32);
  text.setColor(sf::Color::Black);
  text.setString("Akagoria, the revenge of Kalista");

  // put the splash screen in the middle
  sf::FloatRect rect = text.getLocalBounds();
  text.setOrigin(rect.width / 2.0f, rect.height / 2.0f);
  text.setPosition(WIDTH / 2, HEIGHT / 2);
  window.draw(text);

  text.setColor(sf::Color(255, 127, 0));
  text.move(-2.0f, -2.0f);
  window.draw(text);

  window.display();

  /*
   * Initialization
   */

  b2Vec2 gravity(0.0f, 0.0f);
  b2World world(gravity);

  akgr::Database database;
  database.load(AKAGORIA_DATADIR);

  tmx::Map *map = loader.getMap("maps/map.tmx");

  // create systems and add them
  es::Manager manager;
  akgr::createBasicStores(&manager);

  manager.addSystem<akgr::Player>(&manager, &window);
  manager.addSystem<akgr::Stuff>(&manager);
  manager.addSystem<akgr::Physics>(&manager, &world);
  manager.addSystem<akgr::Movement>(&manager);
  manager.addSystem<akgr::Animation>(&manager);
  manager.addSystem<akgr::Render>(&manager, &window, &world, &loader, &database, map);

  manager.initSystems();

  delete map;

  akgr::ArchetypeLibrary library(&loader, &world, &manager);

  /*
   * Add the hero
   */

  es::Entity hero = library.createHero(4000.0f, 4000.0f, 0.0f, "nanims/kalista.nanim");
  auto pos = manager.getComponent<akgr::Position>(hero);
  akgr::HeroPositionEvent event;
  event.pos = pos->body->GetPosition();
  manager.triggerEvent(hero, &event);

  /*
   * Game loop!
   */

  sf::Clock clock;
  while (window.isOpen()) {
    sf::Time elapsed = clock.restart();
    float delta = elapsed.asSeconds();
//     std::printf("FPS: %6.2f        \r", 1./delta);

    manager.updateSystems(delta);
  }

  return 0;
}
