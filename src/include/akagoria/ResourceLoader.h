/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_RESOURCE_LOADER_H
#define AKGR_RESOURCE_LOADER_H

#include <boost/filesystem.hpp>
#include <SFML/Graphics.hpp>
#include <tmx/Map.h>

namespace im {
  namespace bci {
    namespace nanim {
      class Nanim;
    }
  }
}

namespace akgr {
  /*
   * A resource cache for SFML objects.
   */
  template<typename T>
  class SfResourceCache {
  public:

    T *get(const boost::filesystem::path& path) {
      auto it = m_cache.find(path);

      if (it != m_cache.end()) {
        return it->second.get();
      }

      return nullptr;
    }

    T *load(const boost::filesystem::path& path) {
      std::unique_ptr<T> obj(new T);
      bool loaded = obj->loadFromFile(path.string());
      assert(loaded);

      auto inserted = m_cache.emplace(path, std::move(obj));
      assert(inserted.second);

      return inserted.first->second.get();
    }

    T* getOrLoad(const boost::filesystem::path& path) {
      T *obj = get(path);

      if (obj != nullptr) {
        return obj;
      }

      return load(path);
    }

  private:
    std::map<boost::filesystem::path, std::unique_ptr<T>> m_cache;
  };

  class ResourceLoader {
  public:
    ResourceLoader(const boost::filesystem::path& basedir);

    sf::Texture *getTextureFromImage(const boost::filesystem::path& image, bool smooth = true) {
      return getTextureFromImageAbsolute(m_basedir / image, smooth);
    }

    sf::Font *getFont(const boost::filesystem::path& path) {
      return getFontAbsolute(m_basedir / path);
    }

    tmx::Map *getMap(const boost::filesystem::path& path) {
      return getMapAbsolute(m_basedir / path);
    }

    im::bci::nanim::Nanim *getNanim(const boost::filesystem::path& path) {
      return getNanimAbsolute(m_basedir / path);
    }

    sf::Texture *getTextureFromImageAbsolute(const boost::filesystem::path& image, bool smooth);
    sf::Font *getFontAbsolute(const boost::filesystem::path& path);
    tmx::Map *getMapAbsolute(const boost::filesystem::path& path);
    im::bci::nanim::Nanim *getNanimAbsolute(const boost::filesystem::path& path);

  private:
    boost::filesystem::path m_basedir;

    SfResourceCache<sf::Texture> m_textures;
    SfResourceCache<sf::Font> m_fonts;
  };

}

#endif // AKGR_RESOURCE_LOADER_H
