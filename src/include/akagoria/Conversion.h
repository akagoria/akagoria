/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_CONVERSION_H
#define AKGR_CONVERSION_H

#include <Box2D/Box2D.h>
#include <SFML/System/Vector2.hpp>

namespace akgr {

  sf::Vector2f realCoordsFromBoxCoords(const b2Vec2& pos);

  uint16_t bitsFromFloor(int floor);
  uint16_t bitsFromFloor(const std::string& key);

}

#endif // AKGR_CONVERSION_H
