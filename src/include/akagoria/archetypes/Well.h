/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_ARCHETYPES_WELL_H
#define AKGR_ARCHETYPES_WELL_H

#include <es/Manager.h>

#include <akagoria/components/Picture.h>
#include <akagoria/components/Position.h>
#include <akagoria/components/Altitude.h>

namespace akgr {

  struct Well {

    static es::Entity create(es::Manager *manager) {
      es::Entity e = manager->createEntity();

      manager->addComponent(e, Picture::create());
      manager->addComponent(e, Position::create());
      manager->addComponent(e, Altitude::create());

      return e;
    }

  };

}

#endif // AKGR_ARCHETYPES_WELL_H

