/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AKGR_SYSTEMS_RENDER_H
#define AKGR_SYSTEMS_RENDER_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <tmx/Map.h>

#include <akagoria/map/TileMap.h>
#include <akagoria/map/SpriteMap.h>
#include <akagoria/map/ZoneMap.h>

#include "RenderBase.h"

namespace akgr {

  // Renders the sprites on the screen
  class Render : public RenderBase {
  public:
    Render(es::Manager *manager, sf::RenderWindow *window, b2World *world, ResourceLoader *loader, Database *database, tmx::Map *map);

    virtual bool addEntity(es::Entity e) override;
    virtual bool removeEntity(es::Entity e) override;

    virtual void preUpdate(float delta) override;
    virtual void postUpdate(float delta) override;
    virtual void updateEntity(float delta, es::Entity e) override;

  private:
    sf::RenderWindow *getWindow() {
      return m_window;
    }

    es::EventStatus onHeroPosition(es::Entity origin, es::EventType type, es::Event *event);
    es::EventStatus onViewDown(es::Entity origin, es::EventType type, es::Event *event);
    es::EventStatus onViewUp(es::Entity origin, es::EventType type, es::Event *event);

  private:
    sf::RenderWindow * const m_window;
    sf::View m_view;

    int m_floor;

    TileMap m_ground_map;
    TileMap m_lo_tile_map;
    TileMap m_hi_tile_map;
    SpriteMap m_lo_sprite_map;
    SpriteMap m_hi_sprite_map;
    ZoneMap m_zone_map;
  };

}

#endif // AKGR_SYSTEMS_RENDER_H

