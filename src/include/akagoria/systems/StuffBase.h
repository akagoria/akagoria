/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_SYSTEMS_STUFF_BASE_H
#define AKGR_SYSTEMS_STUFF_BASE_H

#include <es/Manager.h>
#include <es/Store.h>
#include <es/GlobalSystem.h>

#include <akagoria/components/Embeddable.h>
#include <akagoria/components/Position.h>

namespace akgr {

  // Take care of every embaddable items
  class StuffBase : public es::GlobalSystem {
  public:
    StuffBase(es::Manager *manager)
      : es::GlobalSystem(15, { Embeddable::type, Position::type }, manager)
      , m_embeddable_store(manager->getStore(Embeddable::type))
      , m_position_store(manager->getStore(Position::type))
    {
    }

    Embeddable *getEmbeddable(es::Entity e) { return m_embeddable_store.get(e); }
    Position *getPosition(es::Entity e) { return m_position_store.get(e); }

  protected:
    const es::ComponentStore<Embeddable> *getEmbeddableStore() const { return &m_embeddable_store; }
    const es::ComponentStore<Position> *getPositionStore() const { return &m_position_store; }

  private:
    es::ComponentStore<Embeddable> m_embeddable_store;
    es::ComponentStore<Position> m_position_store;
  };

}

#endif // AKGR_SYSTEMS_STUFF_BASE_H

