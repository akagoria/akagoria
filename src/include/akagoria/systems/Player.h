/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AKGR_SYSTEMS_PLAYER_H
#define AKGR_SYSTEMS_PLAYER_H

#include <set>
#include <SFML/Graphics/RenderWindow.hpp>

#include "PlayerBase.h"

namespace akgr {

  class InputContext;

  // Handle the player/hero interaction
  class Player : public PlayerBase {
  public:
    enum class Action {
      WALK_FORWARD,
      WALK_BACKWARD,
      TURN_LEFT,
      TURN_RIGHT,
      TAKE_OR_TALK,
    };

    Player(es::Manager *manager, sf::RenderWindow *window);

    // virtual void init() override;
    // virtual void preUpdate(float delta) override;
    virtual void update(float delta) override;
    virtual void postUpdate(float delta) override;

  private:
    es::EventStatus onFloorDown(es::Entity origin, es::EventType type, es::Event *event);
    es::EventStatus onFloorUp(es::Entity origin, es::EventType type, es::Event *event);

  private:
    InputContext *m_ctx;
    sf::RenderWindow * const m_window;
    std::set<Action> m_actions;
  };

}

#endif // AKGR_SYSTEMS_PLAYER_H

