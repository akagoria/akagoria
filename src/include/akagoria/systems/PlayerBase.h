/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_SYSTEMS_PLAYER_BASE_H
#define AKGR_SYSTEMS_PLAYER_BASE_H

#include <es/Manager.h>
#include <es/Store.h>
#include <es/SingleSystem.h>

#include <akagoria/components/Position.h>
#include <akagoria/components/Control.h>
#include <akagoria/components/Motion.h>

namespace akgr {

  // Handle the player/hero interaction
  class PlayerBase : public es::SingleSystem {
  public:
    PlayerBase(es::Manager *manager)
      : es::SingleSystem(11, { Position::type, Control::type, Motion::type }, manager)
      , m_position_store(manager->getStore(Position::type))
      , m_control_store(manager->getStore(Control::type))
      , m_motion_store(manager->getStore(Motion::type))
    {
    }

    Position *getPosition(es::Entity e) { return m_position_store.get(e); }
    Control *getControl(es::Entity e) { return m_control_store.get(e); }
    Motion *getMotion(es::Entity e) { return m_motion_store.get(e); }

  protected:
    const es::ComponentStore<Position> *getPositionStore() const { return &m_position_store; }
    const es::ComponentStore<Control> *getControlStore() const { return &m_control_store; }
    const es::ComponentStore<Motion> *getMotionStore() const { return &m_motion_store; }

  private:
    es::ComponentStore<Position> m_position_store;
    es::ComponentStore<Control> m_control_store;
    es::ComponentStore<Motion> m_motion_store;
  };

}

#endif // AKGR_SYSTEMS_PLAYER_BASE_H

