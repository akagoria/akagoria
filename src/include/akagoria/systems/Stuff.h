/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AKGR_SYSTEMS_STUFF_H
#define AKGR_SYSTEMS_STUFF_H

#include "StuffBase.h"

namespace akgr {

  // Take care of every embaddable items
  class Stuff : public StuffBase {
  public:
    Stuff(es::Manager *manager);


    // virtual void init() override;
    // virtual void preUpdate(float delta) override;
    // virtual void update(float delta) override;
    // virtual void postUpdate(float delta) override;
    // virtual void updateEntity(float delta, es::Entity e) override;

  private:
    es::EventStatus onTake(es::Entity origin, es::EventType type, es::Event *event);

  };

}

#endif // AKGR_SYSTEMS_STUFF_H

