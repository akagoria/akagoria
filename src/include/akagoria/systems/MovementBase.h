/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_SYSTEMS_MOVEMENT_BASE_H
#define AKGR_SYSTEMS_MOVEMENT_BASE_H

#include <es/Manager.h>
#include <es/Store.h>
#include <es/GlobalSystem.h>

#include <akagoria/components/Motion.h>
#include <akagoria/components/Animated.h>

namespace akgr {

  // Select the relevant animation
  class MovementBase : public es::GlobalSystem {
  public:
    MovementBase(es::Manager *manager)
      : es::GlobalSystem(45, { Motion::type, Animated::type }, manager)
      , m_motion_store(manager->getStore(Motion::type))
      , m_animated_store(manager->getStore(Animated::type))
    {
    }

    Motion *getMotion(es::Entity e) { return m_motion_store.get(e); }
    Animated *getAnimated(es::Entity e) { return m_animated_store.get(e); }

  protected:
    const es::ComponentStore<Motion> *getMotionStore() const { return &m_motion_store; }
    const es::ComponentStore<Animated> *getAnimatedStore() const { return &m_animated_store; }

  private:
    es::ComponentStore<Motion> m_motion_store;
    es::ComponentStore<Animated> m_animated_store;
  };

}

#endif // AKGR_SYSTEMS_MOVEMENT_BASE_H

