/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_SYSTEMS_PHYSICS_BASE_H
#define AKGR_SYSTEMS_PHYSICS_BASE_H

#include <es/Manager.h>
#include <es/Store.h>
#include <es/GlobalSystem.h>

#include <akagoria/components/Position.h>
#include <akagoria/components/Motion.h>
#include <akagoria/components/Altitude.h>

namespace akgr {

  // Handle the physics of the world (collision)
  class PhysicsBase : public es::GlobalSystem {
  public:
    PhysicsBase(es::Manager *manager)
      : es::GlobalSystem(20, { Position::type, Motion::type, Altitude::type }, manager)
      , m_position_store(manager->getStore(Position::type))
      , m_motion_store(manager->getStore(Motion::type))
      , m_altitude_store(manager->getStore(Altitude::type))
    {
    }

    Position *getPosition(es::Entity e) { return m_position_store.get(e); }
    Motion *getMotion(es::Entity e) { return m_motion_store.get(e); }
    Altitude *getAltitude(es::Entity e) { return m_altitude_store.get(e); }

  protected:
    const es::ComponentStore<Position> *getPositionStore() const { return &m_position_store; }
    const es::ComponentStore<Motion> *getMotionStore() const { return &m_motion_store; }
    const es::ComponentStore<Altitude> *getAltitudeStore() const { return &m_altitude_store; }

  private:
    es::ComponentStore<Position> m_position_store;
    es::ComponentStore<Motion> m_motion_store;
    es::ComponentStore<Altitude> m_altitude_store;
  };

}

#endif // AKGR_SYSTEMS_PHYSICS_BASE_H

