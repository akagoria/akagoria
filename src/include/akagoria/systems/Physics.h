/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AKGR_SYSTEMS_PHYSICS_H
#define AKGR_SYSTEMS_PHYSICS_H

#include <Box2D/Box2D.h>

#include "PhysicsBase.h"

namespace akgr {

  // Handle the physics of the world (collision)
  class Physics : public PhysicsBase {
  public:
    Physics(es::Manager *manager, b2World *world);

    virtual void updateEntity(float delta, es::Entity e) override;
    virtual void postUpdate(float delta) override;

  private:
    b2World *getWorld() {
      return m_world;
    }

    es::EventStatus onFloorDown(es::Entity origin, es::EventType type, es::Event *event);
    es::EventStatus onFloorUp(es::Entity origin, es::EventType type, es::Event *event);

  private:
    b2World * const m_world;
  };

}

#endif // AKGR_SYSTEMS_PHYSICS_H

