/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_SYSTEMS_RENDER_BASE_H
#define AKGR_SYSTEMS_RENDER_BASE_H

#include <es/Manager.h>
#include <es/Store.h>
#include <es/LocalSystem.h>

#include <akagoria/components/Position.h>
#include <akagoria/components/Picture.h>
#include <akagoria/components/Altitude.h>

namespace akgr {

  // Renders the sprites on the screen
  class RenderBase : public es::LocalSystem {
  public:
    RenderBase(es::Manager *manager, int width, int height)
      : es::LocalSystem(90, { Position::type, Picture::type, Altitude::type }, manager, width, height)
      , m_position_store(manager->getStore(Position::type))
      , m_picture_store(manager->getStore(Picture::type))
      , m_altitude_store(manager->getStore(Altitude::type))
    {
    }

    Position *getPosition(es::Entity e) { return m_position_store.get(e); }
    Picture *getPicture(es::Entity e) { return m_picture_store.get(e); }
    Altitude *getAltitude(es::Entity e) { return m_altitude_store.get(e); }

  protected:
    const es::ComponentStore<Position> *getPositionStore() const { return &m_position_store; }
    const es::ComponentStore<Picture> *getPictureStore() const { return &m_picture_store; }
    const es::ComponentStore<Altitude> *getAltitudeStore() const { return &m_altitude_store; }

  private:
    es::ComponentStore<Position> m_position_store;
    es::ComponentStore<Picture> m_picture_store;
    es::ComponentStore<Altitude> m_altitude_store;
  };

}

#endif // AKGR_SYSTEMS_RENDER_BASE_H

