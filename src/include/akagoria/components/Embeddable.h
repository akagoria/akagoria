/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_COMPONENTS_EMBEDDABLE_H
#define AKGR_COMPONENTS_EMBEDDABLE_H

#include <es/Component.h>
#include <es/Entity.h>

namespace akgr {

  // The component for items that can be taken by the player
  struct Embeddable : public es::Component {

    bool embedded = false;
    es::Entity owner = INVALID_ENTITY;

    static const es::ComponentType type = "Embeddable"_type;

    static Embeddable *create() {
      return new Embeddable;
    }

  };

}

#endif // AKGR_COMPONENTS_EMBEDDABLE_H

