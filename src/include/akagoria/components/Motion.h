/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_COMPONENTS_MOTION_H
#define AKGR_COMPONENTS_MOTION_H

#include <es/Component.h>
#include <akagoria/Commands.h>

namespace akgr {

  // The motion of the entity in the game world
  struct Motion : public es::Component {

    LinearCommand linear = LinearCommand::STOP;
    AngularCommand angular = AngularCommand::STOP;

    static const es::ComponentType type = "Motion"_type;

    static Motion *create() {
      return new Motion;
    }

  };

}

#endif // AKGR_COMPONENTS_MOTION_H

