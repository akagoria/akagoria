/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013-2014, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file has been generated.
 * Do not modify!
 */

#ifndef AKGR_COMPONENTS_CONTROL_H
#define AKGR_COMPONENTS_CONTROL_H

#include <es/Component.h>


namespace akgr {

  // The component for entities that can be controlled
  struct Control : public es::Component {

    bool visible = true;

    static const es::ComponentType type = "Control"_type;

    static Control *create() {
      return new Control;
    }

  };

}

#endif // AKGR_COMPONENTS_CONTROL_H

