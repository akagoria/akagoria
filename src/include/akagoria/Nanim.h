/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_NANIM_H
#define AKGR_NANIM_H

#include <map>
#include <string>

#include <boost/filesystem.hpp>
#include <boost/noncopyable.hpp>
#include <SFML/Graphics/Texture.hpp>

namespace im {
  namespace bci {
    namespace nanim {
      class Nanim;
    }
  }
}

namespace akgr {
  class ResourceLoader;

  class Nanim : public boost::noncopyable {
  public:
    ~Nanim();

    void loadNanim(const boost::filesystem::path& path, ResourceLoader *loader);

    bool updateAnimation(const std::string& name, float dt);
    sf::Texture *getCurrentSprite(sf::IntRect *rect);


  private:
    const im::bci::nanim::Nanim *m_nanim = nullptr;
    std::map<std::string, sf::Texture *> m_textures;

    float m_time = 0.0f;
    int m_current_animation = 0;
    int m_current_frame = 0;

  private:
    void changeCurrentAnimation(const std::string& name);
  };

}

#endif // AKGR_NANIM_H
