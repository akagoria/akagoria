/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_TWEAKS_H
#define AKGR_TWEAKS_H

namespace akgr {

  struct Tweaks {
    constexpr static float BOX2D_SCALE = 0.02f;

    // Input system (Input.cc)
    constexpr static float HOP = 100.;
    constexpr static float TURN = 3.;
    constexpr static float ACCEL = 2.;

    // Graphics system (Graphics.cc)
    constexpr static float BORDER = 200.;

  };

}


#endif // AKGR_TWEAKS_H
