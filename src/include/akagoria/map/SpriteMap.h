/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_MAP_SPRITE_MAP_H
#define AKGR_MAP_SPRITE_MAP_H

#include <vector>

#include <Box2D/Box2D.h>
#include <es/Event.h>
#include <es/EventHandler.h>
#include <es/Manager.h>
#include <SFML/Graphics.hpp>
#include <tmx/Map.h>

#include <akagoria/Database.h>
#include <akagoria/ResourceLoader.h>
#include <akagoria/map/GridMap.h>


namespace akgr {
  struct Sprite {
    int z;
    sf::Vector2f pos;
    sf::IntRect rect;
    sf::Texture *texture;
  };

  class SpriteMap : public sf::Drawable, public sf::Transformable, public GridMap<Sprite> {
  public:
    SpriteMap(ResourceLoader *loader, es::Manager *manager, tmx::Map *map, b2World *world, Database *database, const std::string& kind);

    void addSprite(const Sprite& sprite);

    void update();
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

  private:
    std::vector<const Sprite*> m_sprites;
  };

}


#endif // AKGR_MAP_SPRITE_MAP_H
