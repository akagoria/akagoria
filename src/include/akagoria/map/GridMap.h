/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_MAP_GRID_MAP_H
#define AKGR_MAP_GRID_MAP_H

#include <es/Manager.h>
#include <SFML/System/Vector2.hpp>
#include <simple/matrix.h>
#include <tmx/Map.h>

namespace akgr {

  class GridMapBase {
  public:
    GridMapBase(es::Manager *manager, tmx::Map *map, unsigned unit);

    int getFloor() const {
      return m_floor;
    }

    unsigned getUnitWidth() const {
      return m_unitwidth;
    }

    unsigned getUnitHeight() const {
      return m_unitheight;
    }

    void updateFocus(const sf::Vector2f& pos);
    void updateFocus(unsigned x, unsigned y);

    unsigned getX() const {
      return m_x;
    }

    unsigned getY() const {
      return m_y;
    }

    bool isDirty() const {
      return m_dirty;
    }

    void setDirty() {
      m_dirty = true;
    }

    void setClean() {
      m_dirty = false;
    }

  private:
    es::EventStatus onHeroPosition(es::Entity origin, es::EventType type, es::Event *event);
    es::EventStatus onViewDown(es::Entity origin, es::EventType type, es::Event *event);
    es::EventStatus onViewUp(es::Entity origin, es::EventType type, es::Event *event);

  protected:
    static std::size_t computeGridSize(std::size_t map_size, unsigned unit_size) {
      if (map_size % unit_size == 0) {
        return map_size / unit_size;
      }

      return map_size / unit_size + 1;
    }

  private:
    int m_floor;
    const unsigned m_unitwidth;
    const unsigned m_unitheight;
    unsigned m_x;
    unsigned m_y;
    bool m_dirty;
  };

  template<typename T>
  class GridMap : public GridMapBase {
    static_assert(std::is_copy_constructible<T>::value, "Grid requires copying");
  public:
    GridMap(es::Manager *manager, tmx::Map *map, unsigned unit)
      : GridMapBase(manager, map, unit)
      , m_content(computeGridSize(map->getHeight(), unit), computeGridSize(map->getWidth(), unit))
    {
    }

    bool isValid(unsigned x, unsigned y) {
      return x < m_content.cols() && y < m_content.rows();
    }

    void addObject(const T& obj, const sf::Vector2f& pos) {
      assert(pos.x >= 0.0f);
      assert(pos.y >= 0.0f);

      unsigned x = pos.x / getUnitWidth();
      unsigned y = pos.y / getUnitHeight();

      addObject(obj, x, y);
    }

    void addObject(const T& obj, unsigned x, unsigned y) {
      assert(isValid(x, y));

      m_content(y, x).push_back(obj);
    }

    template<typename Func>
    void processObjects(Func func) {
      unsigned xmin = (getX() > 0) ? getX() - 1 : getX();
      unsigned xmax = (getX() < m_content.cols() - 1) ? getX() + 1 : getX();
      unsigned ymin = (getY() > 0) ? getY() - 1 : getY();
      unsigned ymax = (getY() < m_content.rows() - 1) ? getY() + 1 : getY();

      for (auto x = xmin; x <= xmax; ++x) {
        for (auto y = ymin; y <= ymax; ++y) {
          auto& vec = m_content(y, x);
          for (auto& obj : vec) {
            func(obj);
          }
        }
      }
    }

  private:
    simple::matrix<std::vector<T>> m_content;
  };

}


#endif // AKGR_MAP_GRID_MAP_H
