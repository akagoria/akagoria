/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_MAP_ZONE_MAP_H
#define AKGR_MAP_ZONE_MAP_H

#include <map>
#include <set>
#include <string>

#include <Box2D/Box2D.h>
#include <es/Manager.h>
#include <es/Event.h>
#include <tmx/Map.h>
#include <tmx/Object.h>

namespace akgr {

  class ZoneMap : public b2ContactListener {
  public:
    ZoneMap(b2World *world, es::Manager *manager, tmx::Map *map);

    void addEventZone(const tmx::Object *object, uint16_t floor, const std::string& name, es::EventType event);
    void addCollisionZone(const tmx::Object *object, uint16_t floor);

    virtual void BeginContact(b2Contact* contact) override;

    struct EventZone {
      es::EventType event;
      bool active;
      b2Fixture *fixture;
    };

  private:
    const EventZone *findZone(b2Fixture *fixture) const;
    bool triggerZoneEvent(b2Fixture *fixtureZone, b2Fixture *fixtureOrigin);

    enum class FixtureType {
      SOLID,
      SENSOR,
    };

    b2Fixture *createFixtureFromObject(const tmx::Object *object, uint16_t floor, FixtureType type);
    b2Fixture *createFixtureFromRectangle(const tmx::Rectangle *rect, uint16_t floor, FixtureType type);
    b2Fixture *createFixtureFromPoly(const tmx::PolyBase *poly, uint16_t floor, FixtureType type);

  private:
    b2World * const m_world;
    es::Manager * const m_manager;
    std::set<EventZone> m_zones;
    std::map<std::string, b2Fixture*> m_named_zones;
  };

}

#endif // AKGR_MAP_ZONE_MAP_H

