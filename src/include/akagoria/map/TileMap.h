/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_MAP_TILE_MAP_H
#define AKGR_MAP_TILE_MAP_H

#include <es/Event.h>
#include <es/EventHandler.h>
#include <es/Manager.h>
#include <SFML/Graphics.hpp>
#include <tmx/Map.h>

#include <akagoria/ResourceLoader.h>
#include <akagoria/map/GridMap.h>


namespace akgr {
  struct Tile {
    int z;
    std::array<sf::Vector2f, 4> position;
    std::array<sf::Vector2f, 4> texCoords;
  };

  class TileMap : public sf::Drawable, public sf::Transformable, public GridMap<Tile> {
  public:
    TileMap(ResourceLoader *loader, es::Manager *manager, tmx::Map *map, const std::string& kind);

    void addTile(const Tile& tile);
    void setTexture(sf::Texture *texture);

    void update();
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

  private:
    sf::Texture *m_texture;
    sf::VertexArray m_vertices;
  };

}


#endif // AKGR_MAP_TILE_MAP_H
