/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_CONSTANTS_H
#define AKGR_CONSTANTS_H

#define THIRD_FLOOR     0x0080
#define SECOND_FLOOR    0x0040
#define FIRST_FLOOR     0x0020
#define GROUND_FLOOR    0x0010
#define FIRST_BASEMENT  0x0008
#define SECOND_BASEMENT 0x0004
#define THIRD_BASEMENT  0x0002

#define MAX_FLOOR       THIRD_FLOOR
#define MIN_FLOOR       THIRD_BASEMENT

#endif // AKGR_CONSTANTS_H
