/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_ARCHETYPE_LIBRARY_H
#define AKGR_ARCHETYPE_LIBRARY_H

#include <Box2D/Box2D.h>
#include <es/Manager.h>

#include "ResourceLoader.h"

namespace akgr {

  class ArchetypeLibrary {
  public:
    ArchetypeLibrary(ResourceLoader *loader, b2World *world, es::Manager *manager)
      : m_loader(loader), m_world(world), m_manager(manager)
    {
    }

    es::Entity createHero(float x, float y, float angle, const boost::filesystem::path& nanim);

    es::Entity createBush(float x, float y, float angle, const boost::filesystem::path& img, sf::IntRect rect);
    es::Entity createTree(float x, float y, float angle, const boost::filesystem::path& img, sf::IntRect rect);
    es::Entity createWell(float x, float y, float angle, const boost::filesystem::path& img);

    es::Entity createBall(float x, float y, const boost::filesystem::path& img);

  private:
    b2World *getWorld() {
      return m_world;
    }

    ResourceLoader *getResourceLoader() {
      return m_loader;
    }

    es::Manager *getManager() {
      return m_manager;
    }

  private:
    ResourceLoader *m_loader;
    b2World *m_world;
    es::Manager *m_manager;
  };

}

#endif // AKGR_ARCHETYPE_LIBRARY_H
