/*
 * Akagoria, the revenge of Kalista
 * a single-player RPG in an open world with a top-down view.
 *
 * Copyright (c) 2013, Julien Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AKGR_LOG_H
#define AKGR_LOG_H

#include <cstdarg>
#include <cstdlib>

namespace akgr {

  class Log {
  public:

    enum class Level : int {
      DEBUG   = 0,
      INFO    = 1,
      WARNING = 2,
      ERROR   = 3,
      FATAL   = 4,
    };

    static void setCurrentLevel(Level level);

    static void log(Level level, const char *fmt, va_list ap);

    static void debug(const char *fmt, ...) {
      va_list ap;
      va_start(ap, fmt);
      log(Level::DEBUG, fmt, ap);
      va_end(ap);
    }

    static void info(const char *fmt, ...) {
      va_list ap;
      va_start(ap, fmt);
      log(Level::INFO, fmt, ap);
      va_end(ap);
    }

    static void warning(const char *fmt, ...) {
      va_list ap;
      va_start(ap, fmt);
      log(Level::WARNING, fmt, ap);
      va_end(ap);
    }

    static void error(const char *fmt, ...) {
      va_list ap;
      va_start(ap, fmt);
      log(Level::ERROR, fmt, ap);
      va_end(ap);
    }

    static void fatal(const char *fmt, ...) {
      va_list ap;
      va_start(ap, fmt);
      log(Level::FATAL, fmt, ap);
      va_end(ap);

      std::abort();
    }


  private:

    static Level s_current_level;

    Log() = delete;
    ~Log() = delete;
  };

}


#endif // AKGR_LOG_H
