# Akagoria, the revenge of Kalista

Akagoria is a single-player RPG in an open world with a top-down view.

## Requirements

Akagoria is written in C++11 so you need a C++11 compiler like [Clang](http://clang.llvm.org/) or [GCC](http://gcc.gnu.org/).

Akagoria also needs external libraries:

* [SFML2](http://www.sfml-dev.org/) (zlib/png license), [version 2.1](http://sfml-dev.org/download/sfml/2.1/)
* [libes](https://github.com/jube/libes) (ISC licence), [version 0.4.0](https://github.com/jube/libes/archive/v0.4.0.tar.gz)
* [libtmx](https://github.com/jube/libtmx) (ISC licence), [version 0.2.0](https://github.com/jube/libtmx/archive/v0.2.0.tar.gz)
* [libsuit](https://github.com/jube/libsuit) (ISC licence), [version 0.1.1](https://github.com/jube/libsuit/archive/v0.1.1.tar.gz)
* [Box2D](http://box2d.org/) (zlib/png licence), [version 2.2.1](https://code.google.com/p/box2d/downloads/detail?name=Box2D_v2.2.1.zip)
* [Protocol Buffers](https://code.google.com/p/protobuf/) (new BSD licence), [version 2.6.0](https://protobuf.googlecode.com/svn/rc/protobuf-2.6.0.tar.gz)
* [yaml-cpp](https://code.google.com/p/yaml-cpp/) (MIT licence) [version 0.5.1](https://code.google.com/p/yaml-cpp/downloads/detail?name=yaml-cpp-0.5.1.tar.gz)

## Build and install

You can download the sources directly from gitorious:

    git clone git://gitorious.org/akagoria/akagoria.git

Then you have to use CMake to build the project:

    cd akagoria
    mkdir build
    cd build
    cmake ../src
    make

Finally, you can install the files (you may need root permissions):

    make install

## Authors

- Julien Bernard, julien dot bernard at univ dash fcomte dot fr
- Matteo Cypriani, mcy at lm7 dot fr (Kalista sprite)

## ChangeLog

See [ChangeLog.md](https://gitorious.org/akagoria/akagoria/source/HEAD:ChangeLog.md).

## Website

You can view screenshots of the game on the [Akagoria website](http://www.akagoria.org/).

## Copyright

This game is [free software](http://www.gnu.org/philosophy/free-sw.html) and is distributed under the [GPLv3 licence](http://www.gnu.org/copyleft/gpl-3.0.html), including the assets, except the tools that may have a different licence.
