
Gem::Specification.new do |s|
  s.name        = 'es_generator'
  s.version     = '0.1.0'
  s.date        = '2013-10-08'
  s.summary     = 'ES Generator'
  s.description = 'A generator for entity-system classes'
  s.authors     = ['Julien Bernard']
  s.email       = 'julien dot bernard at univ dash fcomte dot fr'
  s.executables << 'es_make'
  s.files       = [
    'lib/es_generator.rb',
    'lib/es_generator/Archetype.rb',
    'lib/es_generator/Component.rb',
    'lib/es_generator/Event.rb',
    'lib/es_generator/Generator.rb',
    'lib/es_generator/Manager.rb',
    'lib/es_generator/System.rb',
  ]
  s.homepage    = 'http://rubygems.org/'
  s.license     = 'ISC'
end
