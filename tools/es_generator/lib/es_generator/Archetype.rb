# Copyright (c) 2013, Julien Bernard
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'es_generator/Generator'

module ES

  class Archetype < Generator

    attr_reader :name, :components

    def initialize(name, prop)
      @name = name
      @components = prop['components']
    end

    def to_s
      @name
    end

    def header
      <<EOF
#{licence}
#{has_been_generated}
#ifndef AKGR_ARCHETYPES_#{@name.upcase}_H
#define AKGR_ARCHETYPES_#{@name.upcase}_H

#include <es/Manager.h>

#{header_includes}

namespace akgr {

  struct #{@name} {

    static es::Entity create(es::Manager *manager) {
      es::Entity e = manager->createEntity();

      #{header_components_creation}

      return e;
    }

  };

}

#endif // AKGR_ARCHETYPES_#{@name.upcase}_H

EOF

    end

    private

    def header_includes
      @components.map { |c| "#include <akagoria/components/#{c}.h>" }.join("\n")
    end

    def header_components_creation
      @components.map { |c| "manager->addComponent(e, #{c}::create());" }.join("\n      ")
    end

  end

end
