# Copyright (c) 2013, Julien Bernard
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'es_generator/Generator'

module ES

  class Component < Generator

    attr_reader :name, :doc, :fields, :includes

    def initialize(name, prop)
      @name = name
      @doc = prop['doc']
      @fields = prop['fields']
      @includes = prop['includes']
    end

    def to_s
      @name
    end

    #
    # create a header for this component
    #
    def header
      <<EOF
#{licence}
#{has_been_generated}
#ifndef AKGR_COMPONENTS_#{@name.upcase}_H
#define AKGR_COMPONENTS_#{@name.upcase}_H

#include <es/Component.h>
#{header_includes}

namespace akgr {

  // #{@doc}
  struct #{@name} : public es::Component {

    #{header_fields}

    static const es::ComponentType type = "#{@name}"_type;

    static #{@name} *create() {
      return new #{@name};
    }

  };

}

#endif // AKGR_COMPONENTS_#{name.upcase}_H

EOF

    end

    private

    def header_includes
      return "" unless @includes
      @includes.map { |i| "#include <#{i}>" }.join("\n")
    end

    def header_fields
      tmp = @fields.map do |f|
        str = "#{f['type']} #{f['name']}"
        str += " = #{f['default']}" if f.has_key? 'default'
        str += ";"
        str
      end
      tmp.join("\n    ")
    end

  end

end
