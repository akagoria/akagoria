# Copyright (c) 2013, Julien Bernard
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'es_generator/Generator'

module ES

  class System < Generator

    attr_reader :name, :doc, :priority, :needed, :type

    def initialize(name, prop)
      @name = name
      @doc = prop['doc']
      @priority = prop['priority']
      @needed = prop['needed']
      @type = prop['type']
    end

    def to_s
      @name
    end

    def header_base
      <<EOF
#{licence}
#{has_been_generated}
#ifndef AKGR_SYSTEMS_#{@name.upcase}_BASE_H
#define AKGR_SYSTEMS_#{@name.upcase}_BASE_H

#include <es/Manager.h>
#include <es/Store.h>
#include <es/#{@type.capitalize}System.h>

#{header_includes}

namespace akgr {

  // #{@doc}
  class #{@name}Base : public es::#{@type.capitalize}System {
  public:
    #{@name}Base(es::Manager *manager#{ctor_args})
      : es::#{@type.capitalize}System(#{@priority}, { #{header_components_list} }, manager#{ctor_params})
      #{header_stores_init}
    {
    }

    #{header_stores_getter}

  protected:
    #{header_stores_accessor}

  private:
    #{header_stores_field}
  };

}

#endif // AKGR_SYSTEMS_#{@name.upcase}_BASE_H

EOF
    end

    def header
      <<EOF
#{licence}
#ifndef AKGR_SYSTEMS_#{@name.upcase}_H
#define AKGR_SYSTEMS_#{@name.upcase}_H

#include "#{@name}Base.h"

namespace akgr {

  // #{@doc}
  class #{@name} : public #{@name}Base {
  public:
    #{@name}(es::Manager *manager#{ctor_args})
      : #{@name}Base(manager#{ctor_params})
    {
    }

    #{add_entity}
    #{remove_entity}

    // virtual void init() override;
    // virtual void preUpdate(float delta) override;
    // virtual void update(float delta) override;
    // virtual void postUpdate(float delta) override;
    // virtual void updateEntity(float delta, es::Entity e) override;

  };

}

#endif // AKGR_SYSTEMS_#{@name.upcase}_H

EOF
    end

    private

    def ctor_args
      ', int width, int height' if @type == 'local'
    end

    def ctor_params
      ', width, height' if @type == 'local'
    end


    def add_entity
      '// virtual bool addEntity(es::Entity e) override;' if @type == 'local'
    end

    def remove_entity
      '// virtual bool removeEntity(es::Entity e) override;' if @type == 'local'
    end

    def header_includes
      @needed.map { |c| "#include <akagoria/components/#{c}.h>" }.join("\n")
    end

    def header_components_list
      @needed.map { |c| "#{c}::type" }.join(", ")
    end

    def header_stores_init
      @needed.map { |c| ", m_#{c.downcase}_store(manager->getStore(#{c}::type))" }.join("\n      ")
    end

    def header_stores_getter
      @needed.map { |c| "#{c} *get#{c}(es::Entity e) { return m_#{c.downcase}_store.get(e); }" }.join("\n    ")
    end

    def header_stores_accessor
      @needed.map { |c| "const es::ComponentStore<#{c}> *get#{c}Store() const { return &m_#{c.downcase}_store; }" }.join("\n    ")
    end

    def header_stores_field
      @needed.map { |c| "es::ComponentStore<#{c}> m_#{c.downcase}_store;" }.join("\n    ")
    end

  end

end
