# Copyright (c) 2013, Julien Bernard
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'es_generator/Generator'

module ES

  class Manager < Generator

    attr_reader :components, :events, :archetypes, :systems

    def initialize()
      @components = { }
      @events = { }
      @archetypes = { }
      @systems = { }
    end

    def add_component(c)
      @components[c.name] = c
    end

    def add_event(e)
      @events[e.name] = e
    end

    def add_archetype(a)
      @archetypes[a.name] = a
    end

    def add_system(s)
      @systems[s.name] = s
    end

    def to_s
      str = "Manager:\n"
      str += "\tComponents: " + @components.map { |_, c| c.name }.join(", ") + "\n"
      str += "\tEvents: " + @events.map { |_, e| e.name }.join(", ") + "\n"
      str += "\tArchetypes: " + @archetypes.map { |_, a| a.name }.join(", ") + "\n"
      str += "\tComponents: " + @systems.map { |_, s| s.name }.join(", ") + "\n"
      str
    end

    def is_everything_ok

      return true
    end

    def generate_components_header dir
      @components.each do |n, c|
        File.open(File.join(dir, n + '.h'), 'w') do |f|
          f << c.header
        end
      end
    end

    def generate_events_header dir
      @events.each do |n, e|
        File.open(File.join(dir, n + '.h'), 'w') do |f|
          f << e.header
        end
      end
    end

    def generate_systems_base_header dir
      @systems.each do |n, s|
        File.open(File.join(dir, n + 'Base.h'), 'w') do |f|
          f << s.header_base
        end
      end

      @systems.each do |n, s|
        file = File.join(dir, n + '.h')
        unless File.exists? file
          File.open(file, 'w') do |f|
            f << s.header
          end
        end
      end
    end

    def generate_archetypes_header dir
      @archetypes.each do |n, a|
        File.open(File.join(dir, n + '.h'), 'w') do |f|
          f << a.header
        end
      end
    end

    def generate_basic file
      File.open(file, 'w') do |f|
        f << <<EOF
#{licence}
#{has_been_generated}
#include <akagoria/Basic.h>

#{basic_components_header}

namespace akgr {

  void createBasicStores(es::Manager *manager) {
    #{basic_components_create}
  }

}

EOF
      end

    end

    private

    def basic_components_header
      @components.map { |n, _| "#include <akagoria/components/#{n}.h>" }.join("\n")
    end

    def basic_systems_header
      @systems.map { |n, _| "#include <akagoria/systems/#{n}.h>" }.join("\n")
    end

    def basic_components_create
      @components.map { |n, _| "manager->createStoreFor<#{n}>();" }.join("\n    ")
    end

  end

end
