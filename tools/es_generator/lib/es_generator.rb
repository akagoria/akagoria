# Copyright (c) 2013, Julien Bernard
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'yaml'

require 'es_generator/Manager'

require 'es_generator/Archetype'
require 'es_generator/Component'
require 'es_generator/Event'
require 'es_generator/System'

module ES

  def ES.load(basedir)

    return nil unless File.directory? basedir

    manager = Manager.new

    # load components

    components = File.join(basedir, 'components.yaml')

    YAML.load(File.read(components)).each do |name, prop|
      manager.add_component Component.new(name, prop)
    end

    # load events

    events = File.join(basedir, 'events.yaml')

    YAML.load(File.read(events)).each do |name, prop|
      manager.add_event Event.new(name, prop)
    end

    # load archetypes

    archetypes = File.join(basedir, 'archetypes.yaml')

    YAML.load(File.read(archetypes)).each do |name, prop|
      manager.add_archetype Archetype.new(name, prop)
    end

    # load systems

    systems = File.join(basedir, 'systems.yaml')

    YAML.load(File.read(systems)).each do |name, prop|
      manager.add_system System.new(name, prop)
    end

    return manager
  end

end

