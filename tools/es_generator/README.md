# `es_generator`

`es_generator` is a Ruby gem to generate some files related to the Akagoria entity system. It is very specific to Akagoria and may not be very helpful for other projects. It provides a `es_make` command that is necessary for developpers of Akagoria.

## Build and install

To build and install all the necessary files locally, type the following commands:

    gem build es_generator.gemspec
    gem install es_generator-0.1.0.gem

## Authors

- Julien Bernard, julien dot bernard at univ dash fcomte dot fr

## Copyright

This game is [free software](http://www.gnu.org/philosophy/free-sw.html) and is distributed under the the [ISC licence](http://opensource.org/licenses/isc-license).
