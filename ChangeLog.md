# ChangeLog for akagoria

## version 0.1.0 (18 Oct 2013)

- add a new map
- better handling of map
- events for changing floors
- start support for items

## version 0.0.3.1 (19 Dec 2013)

- update libes to 0.4.0 (for events)
- add a simple log system

## version 0.0.3 (15 Dec 2013)

- add a ChangeLog
- add multi-level map handling
- add collision lines in the map

## version 0.0.2 (23 Nov 2013)

- add animations (with Nanim)
- add some trees

## version 0.0.1 (27 Oct 2013)

- add Kalista sprite
- add collisions
- optimize map display

## version 0.0.0 (12 Oct 2013)

- Initial release
