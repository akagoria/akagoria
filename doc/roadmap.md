# Roadmap for the development of the game

This roadmap is subject to changes and may not be complete.

## Code

Status can be: PLANNED, STARTED, DONE

* pack "Dialog": PLANNED
  * Dialog system
  * Translation
  * Sprites: NPC (peasant), Spells, Furniture
* pack "UI": STARTED
  * User interface
  * Minimap
  * Sprites: NPC (merchant), Weapons, Armors
* pack "Items": STARTED
  * Items/Bag
  * Interactions with items
  * Sprites: Chest, Gems, Purse
* pack "Save": PLANNED
  * Save/Load
* pack "Fight": PLANNED
  * Fight system
  * Collision: Bullets
* pack "IA": PLANNED
  * IA for NPC
  * Sprites: NPC (guard)
* pack "Binary": PLANNED
  * Binary release
* pack "Sound": PLANNED
  * Sound

## Sounds and musics

* Sounds
  * Hit with a weapon
  * Crowd (in the village)
  * Country sounds (birds, wind, etc)
* Music
  * One background music for each region (if possible)
  * One background music for each special places (if possible)
