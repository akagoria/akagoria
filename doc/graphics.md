# List of needed graphic item

## Environment

This items are viewed top-down. Priority: medium.

* Well [DONE]
* Barrier
* Haystack
* Cart
* Bridge

## Tiles

This items must be done in tilesets so that they can be used in Tiled. Priority: medium.

* Roads of different types
* Roof tiles with different material and shapes (at least 5 types for the 5 regions)

## NPC

Priority: high.

* Guard
* Merchant
* Sorcerer

## Inventory

This items goes in the inventory. Some can be in the environment too. Priority: low.

* Weapons
* Gems
* Books, Parchment
* Purse
* Breastplate, Leggings, Pauldrons
* Wood
* Ore

## Scenery

This items are viewed top-down. They must be at the scale of the player. Priority: low.

* Tables, Chairs, Furniture
* Chest

## Creature

This items are viewed top-down and need to be animated. Priority: low.

* Dragon

## Spells

* Health [DONE]
* Cure [DONE]
* Protection [DONE]
* Fire [DONE]
* Ice [DONE]
* Lightning [DONE]

## Generated items

* Trees [DONE]
* Bushes [DONE]
* Rocks [IN PROGRESS]

