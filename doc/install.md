# Step by step install of Akagoria

This document describes how to build and install Akagoria and its dependencies for Debian `unstable`.

## Packages in Debian

```sh
apt-get install libsfml-dev libbox2d-dev libprotobuf-dev libyaml-cpp-dev
```

## Build and install `libes`, `libtmx` and `libsuit`

For these three libraries, you can check the documentation:

- [documentation for building `libes`](https://github.com/jube/libes#build-and-install)
- [documentation for building `libtmx`](https://github.com/jube/libtmx#build-and-install)
- [documentation for building `libsuit`](https://github.com/jube/libsuit#build-and-install)

## Build and install Akagoria

Finally, you can download, build and install Akagoria. Check the [general documentation for building Akagoria](https://gitorious.org/akagoria/akagoria/source/HEAD:README.md).

And now, play:

    akagoria

Enjoy!
