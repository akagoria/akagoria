# Akagoria: the revenge of Kalista

## Généralités

«Akagoria: the revenge of Kalista» est un RPG en vue de haut dans un monde ouvert. Les combats ont lieu directement dans l'univers sans séquence spécifique. Le joueur joue Kalista, une sorcière maléfique en quête de ses pouvoirs perdus. L'ambiance est cependant humoristique, détournant les clichés des univers médiévaux-fantastiques.

## Scénario

Le scénario contient cinq parties qui correspondent chacune à la découverte d'une région du monde (et un village). Même s'il y a un scénario global, il est possible d'aller et venir dans l'univers du jeu déjà découvert. Les régions se débloquent au fur et à mesure de l'avancée du jeu.

### Trame de départ

Il y a bien longtemps, les habitants d'Akagoria durent faire face à une grande menace. La Sorcière Kalista sema la terreur et le chaos sur le monde. La Guilde des Mages conçut un puissant sort pour emprisonner Kalista pour l'éternité... pensaient-ils.

Cinq cent quarante-deux ans plus tard, la paix règne sur Akagoria et beaucoup ont oublié ce passé lointain. Mais Kalista se réveille...

### Partie 1 : Arabband

Le jeu commence quand Kalista sort de sa prison. Elle était dans une forme de stase, à l'intérieur d'une prison en ruine, au milieu d'une forêt. Le monde qu'elle a connu a bien changé et elle a perdu tous ses pouvoirs. Ce début de jeu est consacré à la découverte des mécaniques de jeu : dialogue, coffre, combat, magie, etc. Pour chaque mécanique, un petit tutoriel sous forme de texte et une action à effectuer.

Kalista peut alors quitter la prison et ses alentours. Une petite route mène à Arabband, un petit village sympathique. Kalista s'est rendu compte qu'elle ne peut plus utiliser la magie, elle se décide à interroger les villageois à propos de la magie, ils lui conseillent d'aller parler à Guru, le roi d'Akagoria qui se trouve à Elatan, la capitale.

Mais un éboulis coupe la route qui mène à la capitale. Il faut des pioches mais le forgeron Grubee refuse d'en fournir, il campe devant sa maison parce qu'il veut vendre ses pioches mais que personne ne peut les acheter. Les villageois informe Kalista qu'une rivière avec des gemmes se situe non-loin mais que des bêtes en empêchent l'accès. Kalista s'en charge.

Une fois que Kalista amène une gemme au forgeron Grubee, celui-ci lui dit que c'est suffisant pour qu'il puisse vendre les pioches. Les villageois s'occupe de dégager la route et Kalista peut quitter la région.


### Partie 2 : Elatan

Kalista se rend à Elatan, la capitale d'Akagoria. Mais un garde l'empêche de rentrer dans le Palais, résidence de Guru. Il faut qu'elle trouve comment entrer. Elle apprend que Guru est malade depuis de nombreuses semaines, l'empêchant de voir qui que ce soit. En passant par les souterrains du Palais, elle se faufile à l'intérieur. Elle rencontre Mikith, la fille de Guru, qui lui apprend que Guru a été enlevé par un Nécromancien qui demande une rançon.

Kalista part sur la piste du Nécromancien, qui s'avère bien plus fort qu'elle et parvient par la ruse à le neutraliser et à libérer Guru. Celui-ci lui remet un laisser-passer pour pouvoir se rendre au Palais et préfère rentrer incognito. Une fois de retour au Palais, Mikith remercie Kalista (beurk). Guru indique à Kalista qu'il faudrait qu'elle aille à l'Académie de Magie à Inichke.


### Partie 3 : Inichke

Sur la route d'Inichke, elle tombe dans un piège tendu par des voleurs. Elle se retrouve dans une prison. Son voisin de prison l'informe qu'il existe un passage au fond de la cellule qui permet de sortir. En fait, le passage mène dans le cœur du repaire des voleurs. Elle parvient à se débarasser d'eux et à sortir du traquenard.

Kalista se rend à l'Académie de Magie pour savoir comment récupérer son pouvoir. Elle rencontre Tokiann, la Doyenne de l'Académie. Elle apprend l'existence de la Source de Puissance (Source of Power), un endroit légendaire qui renfermerait une grande quantité de magie. Tokiann lui indique qu'une carte menant à cet endroit serait caché près du village d'Orta.


### Partie 4 : Orta

Kalista veut trouver la carte menant à la Source de Puissance. À Orta, elle apprend l'existence de la Grotte Maudite, de laquelle personne n'est jamais revenu. C'est sûrement l'endroit où est la carte. Après être parvenue à la grotte et avoir vaincu ses monstres, Kalista trouve la Clef de Puissance et apprend que la carte est composée de quatre fragments caché dans des coffres partout sur Akagoria gardé par des sphynx.

La première partie est cachée au fond du Lac du Désespoir. La seconde partie est caché dans le Trou Sans Fond. La troisième partie est caché au sommet du Volcan Endormi. La quatrième partie est cachée dans les Falaises Venteuses.


### Partie 5 : Uchun

Kalista est sur la piste de la Source de Puissance. Elle se trouve dans les catacombes de la Grande Citadelle. La clef de la Grande Citadelle est conservée par Niki qui est allé voir sa sœur à Elatan (ou Arabband). Il faut faire un aller-retour !

Une fois la clef en poche, Kalista pénètre dans les catacombes et fouille les souterrains jusqu'au dragon Rroio qui garde la Source de Puissance. Une fois le dragon vaincu, Kalista absorbe toute la magie de la Source. Tokiann apparaît alors et l'informe qu'elle ne pourra désormais que faire le bien grâce à cette source, ce qui représente un véritable calvaire pour Kalista.


## Univers

Dans toutes les villes, il y a des gardes qui interviendront si Kalista tue un habitant. Ils sont généralement armés d'une masse d'arme ou d'une arbalète et sont beaucoup plus fort que Kalista. Le joueur peut ainsi attaquer un habitant mais il risque alors de ne pas survivre très longtemps.

Les éléments commun à chaque ville sont :

* une échoppe pour acheter ou vendre des items
* un panneau indicateur qui pointe la direction des villages alentours


### Arabband

_Arabband_ est un petit village d'Akagoria. Le but de ce village est de faire découvrir les mécaniques du jeu et permet à Kalista de s'équiper un minimum pour rejoindre le village suivant. C'est un village assez excentré du monde d'Akagoria, relié à la capitale par une unique route. Le village est pauvre, il vit essentiellement de la cueillette et des cultures. Les toits des maisons sont en chaume.

PNJ importants : la vieille Cilka, le forgeron Grubee

### Elatan

_Elatan_ est la capitale d'Akagoria. C'est la plus grande ville d'Akarogia, elle paraît assez riche, les toits des maisons sont en tuile rouge. Elle abrite toute l'administration d'Akagoria et tous les notables. Elle dispose de toutes les commodités (qu'on ne retrouve pas systématiquement dans tous les autres villages).

Au centre de la ville se dresse le _Palais Royal_, qui est la demeure du roi d'Akagoria. Il est gardé en permanence et on ne peut y entrer sans laisser-passer royal. Cependant, les égouts permettent de se faufiler à l'intérieur sans être vu. L'intérieur est relativement luxueux par rapport au reste d'Akagoria. Le Palais a deux niveaux, le deuxième niveau étant réservé à la famille royale.

PNJ importants : le roi Guru, sa fille Mikith

### Inichke

_Inichke_ est le deuxième lieu le plus important d'Akagoria, après la capitale. C'est le lieu du savoir et de la connaissance. Ses habitants sont très sensibles à la nature et ont fabriqué leur maison en terre et en bois, y compris les toits. Les artistes sont également nombreux à habiter à Inichke.

Inichke ne serait rien sans son _Académie de Magie_ qui abrite une école de magie et une bibliothèque renfermant tout le savoir magique d'Akagoria. Les élèves de l'Académie sont des privilégiés qui viennent de tout Akagoria. Ils sont peu nombreux à avoir le don de la magie.

PNJ importants : la doyenne Tokiann

### Orta

_Orta_ est un village côtier qui vit principalement de la pêche. Le village vit plutôt bien.

La plus grande fierté d'Orta est le _Phare du Port_ dont la lumière guide les bateaux jusqu'au port. Il culmine sur un promontoire au bord de la mer. Le gardien du phare veille jalousement à ce que personne ne s'approche de la lumière et la maintient en état coûte que coûte.

PNJ importants : le gardien Pagi

### Uchun

_Uchun_ est une ancienne cité militaire fortifiée. Elle en a gardé l'architecture et les toits en granit. Un mur d'enceinte entoure le village par lequel on peut rentrer par quatre portes, une à chaque point cardinal (North Gate, East Gate, West Gate, South Gate). Les portes sont généralement ouverte, sauf la nuit et en cas de danger.

Sur les hauteurs d'Uchun se situe la _Grande Citadelle_, qui abritait autrefois une garnison mais qui est maintenant complètement abandonnée. Elle est assez vaste et se situe sur plusieurs niveaux, dont plusieurs niveaux souterrains. Elle abrite des monstres et personne ne s'y risque sans précaution.


## Quêtes annexes

Il existe trois types de quêtes :

- les quêtes longues qui nécessite de parcourir tout Akagoria pour réussir. Il en existe une par village.
- les quêtes moyennes qui peuvent enchaîner plusieurs intrigues mais qui restent généralement sur une région ou deux. Il en existe une par village.
- les quêtes courtes de services aux habitants. Il en existe plusieurs par villages.

### Quêtes courtes

Les quêtes courtes sont toujours les mêmes :

- Tuer une certaine quantité d'animaux
- Chercher des ressources
- Porter un message à quelqu'un

### Les gemmes de Cilka (quête longue d'Arabband)

But : retrouver les septs gemmes de la vieille Cilka (disséminés sur la carte)

La vieille Cilka, une ancienne magicienne à la retraite, se souvient de tous ses exploits passés et des gemmes qu'elle a pu récolter. Malheureusement, un sort mal exécuté a éparpillé ses gemmes. Elle demande à Kalista si celle-ci peut les retrouver. Les gemmes de Kalista sont au nombre de sept :

* Diamant de Cilka
* Émeraude de Cilka
* Rubis de Cilka
* Saphir de Cilka
* Agate de Cilka
* Grenat de Cilka
* Améthyste de Cilka


### La bâtisse abandonnée (quête moyenne d'Etalan)

But : débarasser la bâtisse de ses occupants gênants

Cette quête permet d'avoir un «logement» avec un coffre pour entreposer les objets de quête.

Kalista aide un vieil acariâtre à débarasser une vieille bâtisse occupée par ses neveux sans son autorisation. En échange, il accepte que Kalista puisse disposer de la bâtisse comme elle le souhaite. La bâtisse est un peu en ruine et assez excentrée des routes et des villages, c'est donc un parfait QG. Une fois la bâtisse dégagée, Kalista découvre un souterrain où les neveux produisaient de l'herbe à fumer. Elle trouve également une liste de trois clients des neveux.

Pour gagner un peu d'argent facile, Kalista doit apporter de l'herbe à fumer aux clients : un se situe à Etalan, un à Arabband et un se situe dans une zone un peu isolée. Ce dernier client est en fait le fournisseur des neveux, il cultive de l'herbe dans son jardin. Devant les menaces de Kalista de révéler son business, il accepte de lui donner une arme magique.

Kalista peut ensuite aller voir un garde pour lui parler du fournisseur et toucher une récompense.


### Le chantage (quête moyenne d'Inichke)

Prérequis : avoir libéré le prisonnier dans le repaire des voleurs
But : faire chanter deux rivaux

Le prisonnier indique qu'il faut qu'il livre une lettre importante d'Aliks à Bog mais qu'à cause du danger, il préfère arrêter. Bog habite à Etalan. La lettre est en fait une lettre de chantage : Aliks menace Bog de révéler qu'il vient jouer son argent en secret au Sloubi dans une cave d'Inichke.

Bog ne nie rien, mais est très embêté. Il demande à Kalista de faire taire Aliks. Pour ça, il lui conseille d'aller voir Osgar à Inichke qui connaît tous les secrets de tout le monde. Osgar se montre assez peu coopératif exigeant un autre secret en échange. En fouillant un peu à Inichke, Kalista apprend que Mikith, la fille du roi, a plein de secrets.

Kalista retourne à Etalan pour obtenir des secrets de Mikith qu'elle s'empresse d'aller répéter à Osgar, qui en échange lui révèle un secret sur Aliks. Kalista peut alors faire chanter à la fois Aliks et Bog pour obtenir de belles récompenses.


## L'héroïne Kalista

Kalista est une sorcière autrefois extrêmement puissante qui a été emprisonnée pendant de très nombreuses années. Elle a une longue chevelure rousse ondulée et est habillée avec une robe noire qui traîne un peu derrière elle. Elle est aigrie et se vexe facilement, surtout quand on se moque d'elle et qu'elle se rend compte qu'elle ne peut plus utiliser ses pouvoirs. Elle retrouve une partie de ses pouvoirs magiques au cours du jeu. Elle est malchanceuse, dès qu'il y a un problème, c'est sur elle que ça tombe. Mais elle est rusée et sait profiter de la faiblesse de ces congénères.

Kalista peut :

* se déplacer sur la carte et entrer dans des lieux clos (maison, grotte)
* parler à d'autres personnages du jeu
* lancer des sorts qu'elle a appris
* être victime d'une maladie, d'un poison ou d'une malédiction
* se munir de diverses pièces d'équipements (plastron, jambières)
* utiliser des couteaux, des épées et des arcs

Kalista possède :

* une jauge de vie (HP) qui se remplit au cours du temps quand rien ne se passe
* une jauge de magie (MP) qui se remplit au cours du temps quand rien ne se passe
* un sac qui peut contenir un nombre fini d'items
* une bourse avec une certaine quantité de pièces d'or
* un journal de bord qui indique où elle se trouve dans ses quêtes



## Personnages secondaires

Par ordre d'apparition.

### Cilka

Doyenne d'Arabband


### Grubee

Forgeron d'Arabband


### Guru et Mikith

Guru est le roi d'Akagoria, il vit dans la ville d'Elatan, la capitale d'Akagoria, dans le Palais. C'est un bon vivant menant Akagoria d'une main douce, comme la plupart de ses prédecesseurs. Il est très apprécié de ses sujets. Il a une fille, Mikith, qui prendra sa succession à sa mort. Mikith est un peu naïve et est très dévouée envers son père. Elle pense que tout le monde est gentil par nature.


### Tokiann

Tokiann est une mage-guerrière, dernière héritière de la Guilde des Mages. Elle attend le retour de Kalista, car la Guilde savait que son sortilège ne serait pas éternel. Tokiann est vielle et ne peut plus combattre Kalista. Elle est la Doyenne de l'Académie de Magie à Inichke.


### Rroio

Rroio est le dragon qui garde la Source de Puissance. Il est grincheux, blasé de devoir garder la Source. Il est encore puissant et n'hésite pas à réduire en cendre quiconque s'approche de la Source.


## Personnages génériques

### Garde

Les gardes sont les garants de la tranquilité des villages. Ils veillent en permanence et interviendront immédiatement s'il y a le moindre problème.

### Marchand

Les marchands vendent les items. Il y en a au moins un par village.


## Systèmes d'évolution et de progression

### Jauges

Kalista, les PNJ et les monstres possèdent deux jauges : une jauge de vie (HP) et une jauge de magie (MP).

Pour les PNJ et les monstres, cette jauge ne peut que descendre, soit à cause d'une blessure suite à une attaque, soit à cause d'une altération. Une fois la jauge de vie à 0, le PNJ ou le monstre meurt.

Quand elle n'est pas sous l'effet d'une altération, Kalista regagne de la vie et de la magie régulièrement. Un repos dans une auberge lui permet de récupérer entièrement mais ne soigne pas ses altérations.

Il existe trois altérations :

* Maladie (Disease) : fait perdre des MP
* Poison (Poison) : fait perdre des HP
* Malédiction (Curse) : fait perdre des MP et des HP

### Expérience

Les actions effectuées rapportent de l'expérience qui permet de passer des niveaux. Chaque niveau augmente le maximum des jauges de vie et de magie.

Les jauges de Kalista commencent à 100 et augmentent de 2 à chaque niveau. À la fin du scénario, Kalista devrait être à peu près au niveau 50, de sorte que ses jauges auront doublées.

### Trophées

Un système de trophées permet de mesurer la progression globale à travers des actions effectuées. Il existe un trophée pour chacune des parties du scénario, un trophée pour chaque quête annexe. Il existe aussi divers trophées en fonction du nombre de créatures tuées, etc.

### Sauvegarde

La sauvegarde du jeu est possible sur des points spécifiques. Il en existe un par village et quelques autres (mais peu) disséminés sur la carte.


## Magie

Kalista et certains PNJ peuvent pratiquer la magie. Ils disposent pour cela de sorts. Kalista peut apprendre des sorts via des parchemins de sort. Le sort X est contenu dans un parchemin nommé «Parchemin de X».

Chaque sort possède :

* une durée de réalisation
* une durée d'action
* une durée de recharge
* un coût en MP
* trois niveaux de puissance

Le niveau du sort augmente en utilisant le sort un certain nombre de fois (avec un effet) pour les sorts de défense, et en tuant des ennemis pour les sorts d'attaques (augmentation du nombre d'XP).

### Sorts de défense

* Santé (Health) : redonne de la vie
  * Niveau 1 : redonne 20 points de vie
  * Niveau 2 : redonne 40 points de vie
  * Niveau 3 : redonne 60 points de vie
* Soin (Cure) : soigne
  * Niveau 1 : soigne les maladies
  * Niveau 2 : soigne les poisons
  * Niveau 3 : soigne les malédictions
* Protection (Protection) : crée un champs de protection de 100 HP
  * Niveau 1 : protège pendant 5 secondes
  * Niveau 2 : protège pendant 10 secondes
  * Niveau 3 : protège pendant 15 secondes

### Sorts d'attaques

* Feu (Fire) : lance une flamme
* Glace (Ice) : lance un pic de glace
* Éclair (Lightning) : lance un éclair


## Items

### Armes

Armes à distance

* Arc
* Arbalète

Armes au corps-à-corps

* Couteau (Knife)
* Épée (Sword)
* Hache d'arme (Axe)

### Potions

* Potion HP
* Potion MP

### Équipements

Les pièces d'équipements d'armure permettent de réduire les dégâts reçus.

* Plastron (Breastplate)
* Jambières (Leggings)
* Spalières (Pauldrons)

### Divers

* Bourse d'or (Purse)
* Coffres à piller (Chest)
* Coffres pour ranger
* Parchemin de sort (Parchment)
* Lettre (Letter)
* Clef (Key)
* Minerai (Ore)
* Bois (Wood)

## Créatures

<!-- Inverser le rôle ? Les créatures à peu près gentilles attaquent Kalista tandis que les créatures présumées méchantes n'attaquent pas Kalista. -->

### Créatures dangereuses

Les créatures dangereuses attaquent quand elles sentent Kalista à proximité.

* Rat
* Renard
* Loups
* Araignée
* Fourmi
* Lézard
* Dragon

### Créatures pacifiques

Les créatures pacifiques n'ont aucune réaction quand Kalista est à proximité. En revanche, elle fuit si elles sont attaquées.

* Chat
* Lapin
* Vache

## Technologies

* SFML2
* Box2D
* TMX (Tiled)
* Nanim


## Nommage

Les personnages féminins ont un k dans leur nom tandis que les personnages masculins ont un g dans leur nom. Les créatures particulières ont un r dans leur nom. Les noms de villes commencent par une voyelle et sont des [noms de villes d'Ouzbékistan](http://www.mithrilandmages.com/utilities/CityNames.php).



<!--
Fujiko
-->
