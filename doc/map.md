# The map in akagoria

This document is a manual for making the map in Akagoria, thanks to [Tiled](http://www.mapeditor.org/).

## Introduction

The map is constructed with different layers for every floor in the game. A floor is composed of two parts: the inside map (representing all the inside zones) and the outside map (representing the outside zones). For each part, here are the layers:

- the ground layer:
- the low tile layer: it's a tile layer with tiles representing things like roads or rivers
- the low sprite layer: it's an object layer with different sprites to represent objects on the map
- the high tile layer: it's a tile layer with tiles representing things like roads or rivers
- the high sprite layer: it's an object layer with different sprites to represent objects on the map
- the zone layer: it's an object layer with all the special zones and the collision lines of the map

Every layer has the following properties:

- `floor`: the floor of the layer (default: 0).
- `kind`: the kind of layer, one value among "ground", "low_tile", "high_tile", "low_sprite", "high_sprite", "zone" (mandatory)


## Ground layer

The ground layer is a tile layer with tiles representing the current biomes. It is composed of a single tileset.

## Low and high tile layers

The low and high tile layers are tile layers representing things like roads or rivers for the low layer and roofs for the high layer. It is composed of a single tileset.

## Low and high sprite layer

The low and high sprite layer are sprite layers representing objects that do not fit well in tiles, like chests for the low layer and trees for the high layer. It is composed of image objects.

## Zone layer

A zone layer is an object layer and is composed of two types of zones: event zones and collision zones.

### Event zone

An event zone is a closed object (rectangle, ellipse or polygon) that has the `event` type and the following properties:

- `type`: the event type that is generated when accessing this zone


### Collision zone

A collision zone is an object that has the `collision` type.

